<div class="cart-summary">
    {% if cart.items_count > 0 %}
        <a href="{{ store.cart_url }}"> {{ "{1}" | translate(cart.items_count ) }} <span
                    class='badge'>{{ cart.total | money }}</span></a>
    {% else %}
        <span> <span class='badge'>0</span></span>
    {% endif %}
</div>