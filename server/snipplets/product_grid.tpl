{% for product in products %}
    {% if loop.index % 4 == 1 %}
        <div class="row">
    {% endif %}

    {% include 'snipplets/single_product.tpl' %}

    {% if loop.index % 4 == 0 or loop.last %}
        </div>
    {% endif %}
{% endfor %}