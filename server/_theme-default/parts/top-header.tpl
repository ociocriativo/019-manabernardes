<div class="top-header">
    <div class="container clearfix">
        <div class="message-wrapper">
            <p class="header-message">
            {% if settings.header_message %}
                {{ settings.header_message }}
            {% else %}
                &nbsp;
            {% endif %}
            </p>
        </div>
        <!-- / .message-wrapper -->
        
        <!-- / .search-content-wrapper -->
        <div class="top-header-rightcolumn clearfix">
            <div class="languages-wrapper">
                {% if languages | length > 1 %}
                    <div class="languages">
                        {% for language in languages %}
                            {% set class = language.active ? "active" : "" %}
                            <a href="{{ language.url }}" class="{{ class }}">{{ language.country | flag_url | img_tag(language.name) }}</a>
                        {% endfor %}
                    </div>
                    <!-- /.languages -->
                {% endif %}
            </div>
            <!-- / .languages-wrapper -->
            <div class="account-wrapper">
                {% if store.has_accounts %}
                    <ul id="auth" class="list-inline">
                        {% if not customer %}
                            {% if store.customer_accounts != 'mandatory' %}
                                <li>{{ "Crear cuenta" | translate | a_tag(store.customer_register_url) }}</li>
                            {% endif %}
                            {{ "Iniciar sesión" | translate | a_tag(store.customer_login_url) }}
                        {% else %}
                            <li>{{ "Mi cuenta" | translate | a_tag(store.customer_home_url) }}</li>
                            <li>{{ "Cerrar sesión" | translate | a_tag(store.customer_logout_url) }}</li>
                        {% endif %}
                    </ul>
                {% endif %}
            </div>
            <!-- / .account-wrapper -->
        </div>
        <!-- / .top-header-rightcolumn -->
    </div>
    <!-- / .top-header -->
</div>
<!-- top-header -->