            <div id="main-menu">
                <nav class="btn-group mobile hidden-md hidden-lg pull-right">
                    <button type="button" id="main_navigation" name="main_navigation" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-align-justify"></i>
                    </button>
                    {% snipplet "navigation-mobile.tpl" %}
                </nav>
                <div id="cart">
                    {% if not store.is_catalog %}
                        {% snipplet "cart.tpl" as "cart" %}
                    {% endif %}
                </div>
                
                <!--MENU-->
                <ul id="menu" class="sf-menu navbar-header list-inline hidden-sm hidden-xs">
                    {% snipplet "navigation.tpl" %}
                </ul>
            </div>