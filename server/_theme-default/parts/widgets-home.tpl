<div class="container">
    <!-- widget zone-->
    <div class="row widgets">

        {% set show_facebook = settings.show_footer_fb_like_box and store.facebook ? 1 : 0 %}
        {% set show_twitter = settings.twitter_widget ? 1 : 0 %}
        {% if show_facebook %}
            <div class="col-md-4 col-sm-4 col-xs-12" style="overflow:hidden;">
                <div class="widget-header">
                    <small>{{ "Facebook" | translate }}</small>
                    <h3>{{ "Síguenos en Facebook" | translate }}</h3>
                </div>
                <div class="widget-divider"></div>
                <div class="widget-content">
                    <div style="margin:10px 0" class="fb-like-box" data-href="{{ store.facebook }}" data-width="980"
                         data-height="200" data-show-faces="true" data-stream="false" data-show-border="false"
                         data-header="false"></div>
                </div>
            </div>
        {% endif %}

        {% if show_twitter %}
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="widget-header">
                    <small>{{ "Twitter" | translate }}</small>
                    <h3>{{ "Síguenos en Twitter" | translate }}</h3>
                </div>
                <div class="widget-divider"></div>
                <div class="widget-content">
                    {{ settings.twitter_widget | raw }}
                </div>
            </div>
        {% endif %}

        {% set newsletter_col = show_facebook and show_twitter ? 4 : (show_facebook or show_twitter ? 8 : 6) %}
        <div class="col-md-{{ newsletter_col }} col-sm-{{ newsletter_col }} col-xs-12">
            {% include 'snipplets/newsletter.tpl' %}
        </div>
    </div>
</div>