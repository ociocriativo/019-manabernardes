{% set has_banner = "banner-home.jpg" | has_custom_image %}
{% set show_help = not (settings.slider | length) and not has_banner and (not has_products or (not sections.primary.products and settings.home_display != "image")) %}
<div class="container {% if show_help %}no-products{% endif %}">
    {% if show_help %}
        <h2>{{ "¡Bienvenido a tu tienda!" | translate }}</h2>
        <div class="no-products-txt">
            {% if has_products %}
                <p>{{ "Todavía no destacaste productos, ¿deseas" | translate }} <a
                            href="/admin/products/feature">{{ "destacar uno ahora" | translate }}</a>?</p>
            {% else %}
                <p>{{ "Todavía no tienes ningún producto, ¿deseas" | translate }} <a
                            href="/admin/products/new">{{ "crear uno ahora" | translate }}</a>?</p>
            {% endif %}
        </div>
    {% endif %}
</div>

{% if settings.slider and settings.slider is not empty and (settings.home_display == "slider" or settings.home_display == "both") %}
    <div class="row-fluid">
        <ul class="homeslider bxslider">
            {% for slide in settings.slider %}
                {% set slide_img = slide.image | static_url %}
                {% if slide.link is empty %}
                    <li><img src="{{ slide_img }}"/></li>
                {% else %}
                    <li><a href="{{ slide.link }}"><img src="{{ slide_img }}"/></a></li>
                {% endif %}
            {% endfor %}
        </ul>
    </div>
{% endif %}

{% if settings.welcome_message %}
    <div class="container">
        <div class="welcomeMessage">
            <h1 class='text-center'>{{ settings.welcome_message }}</h1>
        </div>
    </div>
{% endif %}

<div id="fullMenu">
    <ul id="menu" class="sf-menu navbar-header list-inline">
        {% snipplet "navigation.tpl" %}
    </ul>
</div>

<div class="container products-list page">
    
    {% if show_help or (settings.home_display == "products" or settings.home_display == "both" and sections.primary.products) %}
        {% for product in sections.primary.products %}
            {% if loop.index % 4 == 1 %}
                <div class="row">
            {% endif %}
            
            {% include 'snipplets/single_product.tpl' %}
            
            {% if loop.index % 4 == 0 or loop.last %}
                </div>
            {% endif %}
        {% else %}
            {% if show_help %}
                {% for i in 1..4 %}
                    {% if loop.index % 4 == 1 %}
                        <div>
                    {% endif %}
                    
                    <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="thumbnail">
                            <figure class="image-wrap show-help">
                                <a href="/admin/products/new">{{ 'img/placeholder-product.png' | static_url | img_tag }}</a>
                            </figure>
                            <div class="caption">
                                <h5 class="title"><a href="/admin/products/new">{{ "Producto" | translate }}</a></h5>
                                
                                <p class="price">{{ "$0.00" | translate }}</p>
                            </div>
                        </div>
                    </div>
                    
                    {% if loop.index % 4 == 0 or loop.last %}
                        </div>
                    {% endif %}
                {% endfor %}
            {% endif %}
        {% endfor %}
    {% endif %}
</div>

{% if has_banner %}
    {% set banner_home %}
    <div class="banner widget-static-content">
        <div class="static-content-cover"></div>
        <div class="container">
            <div class="row inside">
                <h2 class="text-center">
                    {{ settings.banner_home_description }}
                </h2>
            </div>
        </div>
    </div>
    {% endset %}
    {% if settings.banner_home_url %}
        <a href="{{ settings.banner_home_url | raw }}">{{ banner_home }}</a>
    {% else %}
        {{ banner_home }}
    {% endif %}
{% endif %}

