{% paginate by 12 %}

{% if "banner-products.jpg" | has_custom_image %}
    {% set banner_products %}
    <div class="banner-innerpage-content category-banner" id="banner-innerpage">
        <div class="banner-innerpage-cover"></div>
        {% if settings.banner_products_description %}
            <div class="container text-banner">
                <div class="row inside">
                    <div class="col-xs-12">
                        <h2 class="banner-copy {% if settings.banner_products_align == "aligncategory_left" %}text-left
                        {% elseif settings.banner_products_align == "aligncategory_right" %}text-right
                        {% else %}text-center{% endif %}">
                            {{ settings.banner_products_description }}
                        </h2>
                    </div>
                </div>
            </div>
        {% endif %}
    </div>
    {% endset %}
    {% if settings.banner_products_url %}
        <a class="banner-innerpage-link" href="{{ settings.banner_products_url | raw }}">{{ banner_products }}</a>
    {% else %}
        {{ banner_products }}
    {% endif %}
{% endif %}

<div id="fullMenu">
    <ul id="menu" class="sf-menu navbar-header list-inline hidden-sm hidden-xs">
        {% snipplet "navigation.tpl" %}
    </ul>
</div>

<div class="container products-list page">
    
    <h1 class="title-product">
        {{ category.name }}
        {% snipplet "breadcrumbs.tpl" %}
    </h1>
    
    {% if products %}
        <div class="product-table">
            {% snipplet "product_grid.tpl" %}
        </div>
    {% else %}
        <div class="centered st">
            {{ "Próximamente" | translate }}
        </div>
    {% endif %}
    {% snipplet "pagination.tpl" %}
</div> <!-- container -->