<div id="fullMenu">
    <ul id="menu" class="sf-menu navbar-header list-inline hidden-sm hidden-xs">
        {% snipplet "navigation.tpl" %}
    </ul>
</div>

<div class="page container">
    <div class="headerBox-Page">
        <h2>{{ page.name }}</h2>
    </div>
    <hr class="featurette-divider">
    <div class="user-content">
        {{ page.content }}
    </div>
</div>