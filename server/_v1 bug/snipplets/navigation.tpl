{% for item in navigation %}
    <li class="full-menu-item {{ item.current ? 'active' : '' }}">
        {% if item.subitems %}
            <a href="{{ item.url }}" {% if item.url | is_external %}target="_blank"{% endif %}>
                {{ item.name }}
                <i class="menu-arrow-down"></i>
            </a>
            <ul class="full-menu-sub">
                {% snipplet "navigation.tpl" with navigation = item.subitems, level = level + 1 %}
            </ul>
        {% else %}
            <a href="{{ item.url }}" {% if item.url | is_external %}target="_blank"{% endif %}>{{ item.name }}</a>
        {% endif %}
    </li>
{% endfor %}