<div class="col-md-3 col-sm-6 col-xs-12">
    <div class="thumbnail">
        <div class="head">
            {% if not product.available %}
                <div class="out-of-stock">{{ "Sin stock" | translate }}</div>
            {% endif %}
            {% if product.compare_at_price %}
                <div class="offer">{{ "Oferta" | translate }}</div>
            {% endif %}
            <figure class="image-wrap">
                <a href="{{ product.url }}" title="{{ product.name }}"
                   class="product-image">{{ product.featured_image | product_image_url("original") | img_tag(product.name) }}</a>
            </figure>
        </div>

        <div class="caption">
            <h5 class="title"><a href="{{ product.url }}" title="{{ product.name }}">{{ product.name }}</a></h5>

            <div class="row">

                <div class="col-md-8  price {% if not product.display_price %}invisible{% endif %}">
                    <span class="price-compare">
                        <span id="compare_price_display"
                              {% if not product.compare_at_price %}class="hidden"{% endif %}>{{ product.compare_at_price | money_nocents }}</span>
                    </span>
                    <span class="price" id="price_display" itemprop="price"
                          {% if not product.display_price %}class="hidden"{% endif %}>{{ product.price | money_nocents }}</span>
                </div>

                <div lass="col-md-4 ">
                    {% if settings.quick_shop %}
                        <a class="hidden-xs hidden-sm product-details-overlay" data-toggle="modal"
                           data-target="#quick{{ product.id }}">
                            <span>{{ settings.quick_shop_label }}</span>
                        </a>
                        {% snipplet "quick-shop.tpl" %}
                    {% endif %}
                </div>

            </div>

        </div>

    </div>
</div>
