<a href="{{ store.cart_url }}">
    <div class="cart-summary">
        {% if cart.items_count > 0 %}
            <span class='badge'>{{ cart.items_count }}</span>
        {% else %}
            <span class='badge'>0</span>
        {% endif %}
    </div>
</a>