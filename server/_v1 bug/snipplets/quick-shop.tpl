<!-- Modal -->
<div class="modal fade" id="quick{{ product.id }}" tabindex="-1" role="dialog" q-hidden="true">
    <div class="quick-content">
        <div class="modal-dialog" data-variants="{{ product.variants_object | json_encode }}">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row" itemscope itemtype="http://schema.org/Product">
                        <div class="col-md-6">

                            <figure class="image-wrap">
                                {% if product.featured_image %}
                                    {{ product.featured_image | product_image_url('original') | img_tag(product.name) }}
                                {% else %}
                                    {{ product.featured_image | product_image_url('original') | img_tag(product.name) }}
                                {% endif %}
                            </figure>

                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-xs-6 col-xs-6 modal-title">
                                    <span><h5>{{ settings.quick_shop_label }}</h5></span></div>
                                <div class="col-xs-1 col-xs-offset-5">
                                    <button type="button" class="close text-right" data-dismiss="modal"
                                            aria-hidden="true">&times;</button>
                                </div>
                                <hr class="divider">
                                <hr>
                            </div>

                            <div class="row">
                                <div class="title col-md-7">
                                    <h2 itemprop="name">{{ product.name }}</h2>
                                </div>

                                <div class="price-holder col-md-5">
                                    <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                        <div class="price {% if not product.display_price %}hidden{% endif %}">
                                            <span class="price-compare text-right">
                                                <span id="compare_price_display"
                                                      {% if not product.compare_at_price %}class="hidden"{% endif %}>
                                                    {{ product.compare_at_price | money }}
                                                </span>
                                            </span><!-- price-compare -->
                                                    
                                            <span class="price text-right" id="price_display" itemprop="price"
                                                  {% if not product.display_price %}class="hidden"{% endif %}>
                                                {{ product.price | money }}
                                            </span>

                                            <meta itemprop="priceCurrency" content="{{ product.currency }}"/>

                                            {% if product.stock_control %}
                                                <meta itemprop="inventoryLevel" content="{{ product.stock }}"/>
                                                <meta itemprop="availability"
                                                      href="http://schema.org/{{ product.stock ? 'InStock' : 'OutOfStock' }}"
                                                      content="{{ product.stock ? 'In stock' : 'Out of stock' }}"/>
                                            {% endif %}
                                        </div>
                                        <!-- price -->
                                    </div>
                                </div>
                                <!-- price-holder -->
                            </div>

                            <hr class="divider">

                            <meta itemprop="image"
                                  content="{{ 'http:' ~ product.featured_image | product_image_url('medium') }}"/>
                            <meta itemprop="url" content="{{ product.social_url }}"/>
                            {% if page_description %}
                                <meta itemprop="description" content="{{ page_description }}"/>
                            {% endif %}

                            <form id="product_form" method="post" action="{{ store.cart_url }}">
                                <input type="hidden" name="add_to_cart" value="{{ product.id }}"/>

                                {% if product.variations %}
                                    <div class="fancyContainer">
                                        <ul class="list-inline">
                                            {% for variation in product.variations %}

                                                <li class="attributeLine">
                                                    <label class="variation-label"
                                                           for="variation_{{ loop.index }}">{{ variation.name }}</label><br>
                                                    <select class="btn btn-default dropdown-toggle"
                                                            id="variation_{{ loop.index }}"
                                                            name="variation[{{ variation.id }}]"
                                                            onchange="LS.changeVariant(changeVariant, '#quick{{ product.id }} .modal-dialog');">
                                                        {% for option in variation.options %}
                                                            <option value="{{ option.id }}"
                                                                    {% if product.default_options[variation.id] == option.id %}selected="selected"{% endif %}>{{ option.name }}</option>
                                                        {% endfor %}
                                                    </select>
                                                </li>
                                            {% endfor %}
                                        </ul>
                                    </div>
                                {% endif %}

                                <hr class="divider">
                                <div class="addToCartButton">
                                    {% set state = store.is_catalog ? 'catalog' : (product.available ? product.display_price ? 'cart' : 'contact' : 'nostock') %}
                                    {% set texts = {'cart': "Agregar al carrito", 'contact': "Consultar precio", 'nostock': "Sin stock", 'catalog': "Consultar"} %}
                                    <input type="submit" class="btn btn-primary btn-lg addToCart {{ state }}"
                                           value="{{ texts[state] | translate }}"/>
                                </div>
                            </form>

                            <hr>
                            <div class="description user-content">
                                {% if product.description != '' %}
                                    {{ product.description | plain | truncateWords(50) }} ...
                                    <a class="quick" href="{{ product.url }}"
                                       title="{{ product.name }}">{{ "Ver descripción completa" | translate }}</a>
                                {% endif %}
                            </div>
                        </div>
                        <!-- col-md-6 -->
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div><!-- /.modal -->

