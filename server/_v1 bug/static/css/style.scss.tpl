//VARIABLES
$primary-color: {{ settings.primary_color }};
$secondary-color: {{ settings.secondary_color }};
$black-color: darken($primary-color, 25);
$white-color: lighten($primary-color, 74);
$banner-category-color: {{ settings.bannercategory_color_text }};
$banner-contact-color: {{ settings.bannercontact_color_text }};

$fonttxt: {{ settings.font_rest | raw }};
$fontnav: {{ settings.font_navigation | raw }};
$fonthead: {{ settings.font_headings | raw }};
$fontbut: {{ settings.font_buttons | raw }};

$primary-color-light: lighten( $primary-color, 10% );
$primary-color-lighter: lighten( $primary-color, 20% );
$primary-color-dark: darken( $primary-color, 10% );
$primary-color-darker: darken( $primary-color, 20% );

$secondary-color-light: lighten( $secondary-color, 10% );
$secondary-color-lighter: lighten( $secondary-color, 20% );
$secondary-color-dark: darken( $secondary-color, 10% );
$secondary-color-darker: darken( $secondary-color, 20% );

$black-color-softer: lighten( $black-color, 97% );
$black-color-soft: lighten( $black-color, 90% );
$black-color-light: lighten( $black-color, 80% );
$black-color-lighter: lighten( $black-color, 60% );


.homeslider {
    position: relative;
    padding: 0;
    margin: 0;
}

.homeslider li {
    padding: 0;
    margin: 0;
    -webkit-background-size: 100% auto;
    -moz-background-size: 100% auto;
    -o-background-size: 100% auto;
    -ms-background-size: 100% auto;
    background-size: 100% auto;
    background-position-y: -75px;
    width: 100%;
}

.homeslider li img {
    width: 100%;
}

//MIXINS
@mixin vertical-align {
  position: relative;
  top: 50%;
  -webkit-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}

@mixin opacity($opacity) {
  opacity: $opacity ;
  $opacity-ie: $opacity * 100;
  filter: alpha(opacity=$opacity-ie); //IE8
}

/************************************************************************************
DEFAULTS
*************************************************************************************/
body {
    color: $primary-color;
    font-family: $fonttxt!important;
    background-color: #FFF;
}

a, active, a:hover, a:focus {
    color: $secondary-color;
    text-decoration: none;
    outline: 0;
}

h1, h2, h3, h4 {
     font-weight: 300;

}
::-moz-selection {
    color: $white-color;
    background: $secondary-color;
}
::selection   {
    color: $white-color;
    background: $secondary-color;
} 

.hidden { display:none; }
.l { text-align: left; }
.c { text-align: center; }
.r { text-align: right; }

/************************************************************************************
COLOR SCHEME
*************************************************************************************/
.action-color {
    color: $secondary-color;
}
/************************************************************************************
HEADER
*************************************************************************************/
.navbar > .container .navbar-brand {
    margin-left: 0;
}
.navbar-non-static {
    padding-top: 41px;
}

.no-products {
    text-align: center;
}

.welcomeMessage {
    margin-top: 80px;
}
.top-header {

	background: $primary-color;
	color: $white-color !important;

    .message-wrapper,
    .account-wrapper,
    .languages-wrapper,
    .search-content-wrapper {
        display: block;
        margin-left: 1%; 
    }
    .message-wrapper,
    .account-wrapper,
    .languages-wrapper {
       margin-top: 2px; 
    }

    .message-wrapper {
        float: left;
        margin-left: 0; 
    }
    .search-content-wrapper {
        width: 30%;
        float: right;
    }
    .languages-wrapper {
        // width: 5%;
        float: right;
         margin-left: 2%; 
    }
    .account-wrapper {
        // width: 20%;
        float: right;
    }

    a {
        color: rgba( $white-color, .4 );

        &:hover {
            color: $secondary-color;
        }
    }

    p {
        padding-top: 11px;
    }

    .search-wrapper {
        padding:8px 0 6px;
        .form-control {
            border:0;
        }
    }

    .col {
        margin: 0;
        padding-left: 0;
        padding-right: 0;
    }

    .languages, #auth {
        padding: 11px 0 4px;
        text-align: right;
    }   
    #auth  {
        li {
            margin-left: 8px;
            &:first-child {
                margin-left: 0;
            }
        }
    } 
}

.navbar-toggle {
    outline: 0;
}

.cart-summary {
    text-align: right;
    a {
        padding: 0 0 30px 0;
    }
}

#logo {
    float: left;

    a {
        display: block;
        overflow: hidden;

        img {
            max-height: 32px;   
        }  
    }
}

#no-logo {
    font-size: 30px;
    font-weight: bold;
    color: $primary-color;
}

#menu {
    width: auto;
    text-align: right;
    float: right;
    margin-right: 40px;

    &.list-inline  {
        
        li {
        margin: 0;
        margin-right: -2px;
        padding: 0 !important;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        }

        ul li {
            margin-right:0 !important;

        }
    }
}


/************************************************************************************
PRODUCTS
*************************************************************************************/
.products-header {
    border-bottom: solid 1px #CCC;
    height: 25px;
    margin: 0px 0px 20px 0px;
}

.products-header .breadcrumbs-wrapper {
    float: right;
    margin: 0px 0px 0px 0px;
}

.products-list {

    .products-header {

        small {
            text-transform: uppercase;
            letter-spacing: 1.25px;
            font-size: 80%;
            color: $black-color-lighter;
        }
        h3 {
            font-size: 16px;
            color: #666;
            text-transform: uppercase;
            line-height: 25px;
            height: 25px;
            padding: 00px;
            margin: 0px 0px 0px 15px;
            float: left; 
            
        }
    }
}
.product-details-overlay {
    text-align: right !important;
    padding-left: 0;
    cursor: pointer;

    &:hover {
        text-decoration: underline !important;
    }
}

.social-container-box {
    .shareLinks {
    }
}

.product-description-wrap {
    margin: 60px 0;

    .product-description-title {
        font-weight: 300;
        color: $secondary-color;
        margin-bottom: 20px;
    }

}
.products-header {

    .title {
        h2 {
            font-size: 16px;
            color: #666;
            text-transform: uppercase;
            line-height: 25px;
            height: 25px;
            padding: 0px;
            margin: 0px 0px 0px 15px;
            float: left; 
        }
    }
}

.image-wrap {
    max-height: 280px;
    overflow: hidden;
    max-width: 800px;       
    -webkit-transition: max-width .3s ease-out;  /* Saf3.2+, Chrome */
    -moz-transition: max-width .35s ease-out;  /* FF4+ */
    -ms-transition: max-width .3s ease-out;  /* IE10? */
    -o-transition: max-width .53s ease-out;  /* Opera 10.5+ */
    transition: max-width .3s ease-out;
    background-color: $secondary-color;

    img {
            width: 100%;
            -webkit-transition: margin-top .2s ease-out;  /* Saf3.2+, Chrome */
            -moz-transition: margin-top .2s ease-out;  /* FF4+ */
            -ms-transition: margin-top .2s ease-out;  /* IE10? */
            -o-transition: margin-top .2s ease-out;  /* Opera 10.5+ */
            transition: margin-top .2s ease-out;
        }

    a {
        img {
        -webkit-transition: all 0.3s ease;                  
        -moz-transition: all 0.3s ease;                 
        -o-transition: all 0.3s ease;   
        -ms-transition: all 0.3s ease;          
        transition: all 0.6s ease;
        -webkit-transform:translate3d(0,0,0);
        -moz-transform:translate3d(0,0,0);
        }
        &:hover img {
            opacity: .5;
        }
    }
    &.show-help {
        background-color: $black-color-soft;
    }

}
     
@media only screen and (min-width: 1160px) {
    .image-wrap { max-width: 1000px; }
}

.thumbnail {
    background-color: $white-color;
    border: none;
    border-radius: 0;
    
    display: block;
    height: 300px;
    
    line-height: 1.42857;
    margin-bottom: 0px;
    padding: 0;
    transition: all 0.2s ease-in-out 0s;
}

    .offer {
        position: absolute;
        display: inline-block;
        top: 0;
        z-index: 10;
        background: $secondary-color;
        text-transform: uppercase;
        font-weight: bold;
        color: $white-color;
        padding: 5px 8px;
    }
    .out-of-stock {
        position: absolute;
        display: block;
        text-align: center;
        z-index: 10;
        background: $primary-color;
        text-transform: uppercase;
        font-weight: bold;
        color: $white-color;
        padding: 5px 8px;
        height: 30px;
        width: 100px;
        top: 44%;
        margin-top: -30px;
        left: 50%;
        margin-left: -50px;
    }

    .caption {

        color: $primary-color;

        p {
            color: $black-color-lighter;
        }

        .title {
            margin: 4px 0;
            font-weight: 300;
            letter-spacing: .5px;

            a {
                color: $primary-color-light;
            }
        }

        .price {
            color: $secondary-color;
            font-size: 16px;
            font-weight: bold;
        }

        .price-before {
            text-decoration: line-through;
            color: $black-color-lighter;
            font-weight: normal;
        }
    }
}

.cloud-zoom-wrap {
    #zoom img {
        max-width: 100%;
    }
    .mousetrap {
        display: block;
        visibility: visible;
    }
}

.pagination>li>a, .pagination>li>span {
    color: $primary-color;
}

.pagination>li>a:hover, .pagination>li>span:hover {
    color: $white-color;
    background-color: $secondary-color;
}

/************************************************************************************
SINGLE PRODUCTS PAGE
*************************************************************************************/
#breadcrumb {
    color: $black-color-lighter;

    .crumb {
        color: $black-color-lighter;

        &:hover {
            color: $secondary-color;
        }

        &.last {
            color: $secondary-color;
            
        }
    }
}

.title-wrapper {
    margin: 20px 0;

    .title {
        width: 100%;
        margin-right: 5%;
        float: left;
    }

    .price-holder {
        font-size:30px;
        line-height: 100%;
        font-weight: 300 !important;
        float: right;
        text-align: left;
        min-width: 30%;
        
        .price {
            display: inline-block;
            vertical-align: bottom;
            text-align: right;
            #price_display {
                font-weight: 300;
            }
            .price-compare {
                display: block;
                vertical-align: bottom;
                #compare_price_display {
                    font-weight: 300 !important;
                    font-size: 22px;
                }
              
            }
        }
    }
 }

.mousetrap{
    max-width: 100%;
 }
 .cloud-zoom-big {
    margin-left: 15px;
 }

/************************************************************************************
404 PAGE
*************************************************************************************/



/************************************************************************************
PAGE
*************************************************************************************/
.page {
    margin-top: 20px;

    .banner {
        padding: 0;
        position: relative;
        margin-top: 40px;
        overflow: hidden;

        & .text-banner {
            display: table;
            position: absolute;
            height: 200px;
            width: 100%;
            padding: 5%;
            text-align: center;

            
            & .banner-copy {
                display: table-cell;
                vertical-align: middle;
                font-weight: 300;
                line-height: 110%;
            }

        }

        img {
          width: 100%;  
        }
    }
}

/************************************************************************************
CARRITO
*************************************************************************************/
#shoppingCartPage {

    ul {
        list-style: none;
    }

    .contentBox {
        padding:20px 30px;
        width:880px;
    }
    .contentBox {
        & a {
        color:#535353;
        } 
    }
    .cart-contents {
        margin: 30px 0;
    }

    .cart-contents {
        margin: 40px 0;

        ul {
            overflow: hidden;
            list-style: none;

            li {
                float: left;
                margin:0;

                &.last {
                    padding-right: 0;
                }
            }
        } //ends cart-contents ul

        .firstrow {
            
            font-size: 16px;
            font-weight: bold;
            text-align: right;
            margin: 0;
            border-bottom: 3px solid $black-color-soft;
            padding: 6px 0;
            // padding: 0 0 10px 0;

            li {
                // height:20px;

                &.span6 {
                    width:55%;
                }
            }
            .col-image-product, .col-product {
                text-align: left;
            }
        }  //ends firstrow 

        .productrow {
            padding: 10px 0;
            margin: 0;
            border-top: 1px solid $black-color-soft;

            &:hover {
                background: $black-color-softer;
            }

            a {
                font-size:16px;
            }

            li {
                font-size: 18px;
                line-height:22px;
                margin-top:35px;
                text-align: right;
            }

            .col-image {
                margin-top:0;
                img {
                    max-height: 88px;
                    max-width: 88px;
                }
            }
            .col-product {
                margin-top:0;
                text-align: left;
            }
            .col-delete {
                text-align: center;

                a {
                    color: $primary-color;

                   &:before {
                    content: '\f00d';
                    font-family: FontAwesome;
                    display: inline-block;
                    color: $primary-color;                        
                    } 

                    &:hover:before {
                        color: $secondary-color;
                    }

                }
                
            }
            .thumb{
                float:left;
            }
            .name {
                margin-top:35px;
                margin-left:10px;
                display:inline-block;
                color: $primary-color;
                font-weight: bold;

                &:hover {
                    color: $secondary-color;
                }
            }
            .col-quantity {
                input {
                    border:1px solid #D2D2D2;
                    color:#353535;
                    font-size:16px;
                    padding:1px;
                    text-align:center;
                    width:25px;
                    margin: 0px;
                }
            }
            .col-delete {
                a {
                    font-size:18px;
                }
            }
        }//ends productrow

        .total-price {
            text-align: right;
            font-weight: 300;
            font-size: 22px;
            padding: 20px;
            border-top: 1px solid $black-color-light;
            background: $black-color-soft;
            color: $primary-color;
        }//ends total-price

        .go-to-checkout{
            text-align:right;
            margin: 20px 10px;
            overflow:hidden;

            input {
                margin-left:12px;
                padding: 11px 20px;
                float:right;
            }
        }

     } //ends cart-contents

     .checkOutCallOuts {
        & .btn {
            margin-left: 20px;
        }
    }

    #shipping-calculator {
        padding: 20px;
        margin-bottom: 60px;
        border-top: 1px solid $black-color-light;
        background: $primary-color-dark;
        color: $white-color;

        ul.list-inline {
            display: inline-block;
            margin: 0 auto;

           & > li {
            line-height: 2.2em;
           }
        }

        ul.shipping-list > li {
            margin-top:20px;
        }

        .shipping-method-name {
            margin-left: 10px;
        }

        .loading {
            font-size: 20px;
        }

        .loading, .alert {
            vertical-align: super;
        }
    }

}

#change-quantities{
    display:none;
}

/************************************************************************************
SLIDER
*************************************************************************************/
.slider-wrapper {
    margin: 0 auto;
    max-width: 100%;
    text-align: center;
}

.bjqs-slide {
    width: 100%,
    img {
        min-width: 100%;
    }
}

/************************************************************************************
MENU
*************************************************************************************/
.navbar-nav a {
    display: block;
    position: relative;
    color: #7D7D7D;
    font-size: 17px;
    font-weight: 300;
    height: 20px;
    line-height: 20px;
    padding: 13px 18px;
    text-decoration: none;
    text-transform: uppercase;
}

.navbar {
    border-radius: 0 !important;
    margin-bottom: 0;
    border: none;
    border-bottom: 1px solid $black-color-lighter;

    .cart-summary {
        a {
           color: $primary-color; 

           &:hover {
            color: $secondary-color; 
           }
        }

        .badge {
            background: $secondary-color;
            padding: 4px 7px 5px;
            font-size: 13px;
            margin-left: 5px;
        }
        
     }

    .fa-shopping-cart {
        color: $primary-color;
        margin-right:5px;
    }  
}

.navbar {
    -webkit-transition: all 0.3s ease;                  
    -moz-transition: all 0.3s ease;                 
    -o-transition: all 0.3s ease;   
    -ms-transition: all 0.3s ease;          
    transition: all 0.6s ease;
    padding-top: 15px;

    & .container {
    }

    &.navbar-default .navbar-toggle {
        border-color: $primary-color;
    }

    &.navbar-toggle {
        background-image: none;
        border: 1px solid rgba(0, 0, 0, 0);
        border-radius: 4px;
        float: right;
        margin-bottom: 8px;
        margin-right: 15px;
        margin-top: 8px;
        padding: 9px 10px;
        position: relative;
    }
    &.navbar-default .navbar-toggle .icon-bar {
        background-color: $primary-color;
    }
    &.navbar-default .navbar-toggle:hover, 
    &.navbar-default .navbar-toggle:focus {
        background-color: $secondary-color;
        border-color: $secondary-color-dark;
    }
    &.navbar-default .navbar-toggle:hover .icon-bar {
        background-color: $white-color;
    }
}

.navbar-default {
    background-color: transparent;
    border-color: rgba( $black-color, .1 );
}

.affix {
    top: 0; /* Set the top position of pinned element */
    left: 0;
    right:0;
    margin: 0 auto !important;
    background: rgba( $white-color, .9 );
    padding-top: 5px; /* Set the top padding to reduce the nav height */
    // z-index: 99991;

    & .container{
        padding-top: 0;
        // height: 100px;
    }

    .container .navbar-brand .navbar-header & {
        padding-top: 0 !important;
        // height: 100px;
    }

    & .navbar-brand {
        margin-top: 10px;
    }

    & #logo {
        margin-top: 10px;

        & #no-logo {
            position: absolute;
            margin-top: -10px !important;
        }
    }

    & #main-menu {
        padding-top: 10px;
    }
}

/************************************************************************************
MODAL
*************************************************************************************/
.modal-dialog {
    width: 80%;
    margin: 10px 10%;

    .modal-body {
        img {
            max-width: 100%;
            height: auto;
        }
    }

    .modal-header {
        ul {
            margin: 0;
            overflow: hidden
        }
    }
    .modal-title {
        color: $secondary-color;
    }

    .title {
        h2 {
            margin: 0;
        }
    }

    .price {
        text-align: right;

        #price_display {
            font-size: 30px;
            font-weight: 300;
            line-height: 100%;
        }
        #compare_price_display {
          font-size: 22px;
          font-weight: 300 !important;
          margin-right: 10px;
        }
    }

    .image-wrap {
        background: #FFF !important;
        max-height: 500px;
        overflow: hidden;
        max-width: 800px;       
        -webkit-transition: max-width .3s ease-out;  /* Saf3.2+, Chrome */
        -moz-transition: max-width .35s ease-out;  /* FF4+ */
        -ms-transition: max-width .3s ease-out;  /* IE10? */
        -o-transition: max-width .53s ease-out;  /* Opera 10.5+ */
        transition: max-width .3s ease-out;
        
        img {
                width: 100%;
                -webkit-transition: margin-top .2s ease-out;  /* Saf3.2+, Chrome */
                -moz-transition: margin-top .2s ease-out;  /* FF4+ */
                -ms-transition: margin-top .2s ease-out;  /* IE10? */
                -o-transition: margin-top .2s ease-out;  /* Opera 10.5+ */
                transition: margin-top .2s ease-out;
            }

        a {
            img {
            -webkit-transition: all 0.3s ease;                  
            -moz-transition: all 0.3s ease;                 
            -o-transition: all 0.3s ease;   
            -ms-transition: all 0.3s ease;          
            transition: all 0.6s ease;
            -webkit-transform:translate3d(0,0,0);
            -moz-transform:translate3d(0,0,0);
            }
            &:hover img {
                opacity: 0.9;
            }
        } 
    }
}

/************************************************************************************
WIDGETS
*************************************************************************************/
.widgets {

    margin-bottom: 40px;

    .newsletter {
        .btn {
            margin-top: 15px;
        }

    }

    .widget-header {

        small {
            text-transform: uppercase;
            letter-spacing: 1.25px;
            font-size: 80%;
            color: $black-color-lighter;
        }
        h3 {
            margin: 4px 0 40px;
        }
    }
    .widget-divider {
        width: 30px;
        height: 1px;
        background: $black-color-light;
        margin: 40px 0 20px;
    }
}
/************************************************************************************
FORMS
*************************************************************************************/

.form-control {
    background-color: $white-color;
    background-image: none;
    border-radius: 0;
    border-color: $black-color-light;
    box-shadow: none;
    color: $black-color-lighter;
    display: block;
    font-size: 12px;
    height: 34px;
    line-height: 1.42857;
    padding: 4px 10px;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
    vertical-align: middle;
    width: 100%;
    margin-bottom: -16px;

}
.form-control:focus {
    border-color: $black-color-light;
    border-left: 2px solid $secondary-color;
     color: $primary-color;
    -webkit-box-shadow: none;
    box-shadow: none;
}

.form-control:-moz-placeholder,
.form-control::-moz-placeholder,
.form-control:-ms-input-placeholder,
.form-control::-webkit-input-placeholder {
  color: $primary-color;
}

/************************************************************************************
PAGE-PRODUCT
*************************************************************************************/
.page-product {

    .shareLinks {
        *:before {
            content: none;
        }
    }
    .installments {
        text-align: right;
        margin: 0 0 40px;

        #installments_number, #installments_amount {
            font-weight: bold;
            color: $secondary-color;
        }

    }
    .jThumbnailScroller {
        margin-top: 20px;
    }
}

/************************************************************************************
PAGE-CONTACT
*************************************************************************************/
.page-contact {

    .form {
        .btn {
            margin-top: 15px;
        }
    }

    .form-control {
        padding: 15px;
        font-size: 15px;
        font-weight: normal;
        height: auto !important;
        color: $black-color-lighter;

        &:focus {
            border-color: $black-color-light;
            border-left: 2px solid $secondary-color;
            color: $primary-color;
            outline: 0;
            -webkit-box-shadow: none;
            box-shadow: none;
        }
    }

    *:before {
        margin-right: 10px;
        color: $primary-color;
    }
    *:hover:before {
        color: $secondary-color;
        & > a {
            color: $secondary-color;
        }
    }

    ul li {
        color: $primary-color;
        & a {
            color: $primary-color;
        }
        &:hover > a {
             color: $secondary-color;
        }
    }

    .phone {
        &:before {
            content: '\f095';
            font-family: FontAwesome;
            display: inline-block;
            font-size: 16px;
            margin-right: 13px;
        }
    }
    .mail {
        &:before {
            content: '\f003';
            font-family: FontAwesome;
            display: inline-block;
            font-size: 15px;
        }
    }
    .blog {
        &:before {
            content: '\f086';
            font-family: FontAwesome;
            display: inline-block;
            font-size: 16px;
        }
    }
    .address {
        &:before {
            content: '\f041';
            font-family: FontAwesome;
            display: inline-block;
            font-size: 16px;
            margin-right: 16px;
        }
    }
}
.alert {
    border: 1px solid rgba(0, 0, 0, 0);
    border-radius: 0 !important;
    margin-bottom: 20px;
    padding: 15px;
}

.sidebar {
    margin-top:30px;
}

#google-map {
    margin-bottom:40px;
    width: 100%;
    height: 250px;

    img {
        max-width:none;
    }
}
/************************************************************************************
BANNER
*************************************************************************************/
#banner-innerpage {
    color: $white-color;
    min-height: 200px;

    .banner-copy {
        margin: 20px 0;

        a {
            color: $white-color;
        }
    }

    .text-banner {
        height: 200px;
        display: table;
    }

    &.banner-innerpage-content {
        position: relative;
        text-transform: inherit;
    }
    &.banner-innerpage-content .banner-innerpage-cover {
        background-position: center center;
        background-repeat: no-repeat;
        background-size: 100% auto;
        height: 100%;
        position: absolute;
        top: 0;
        width: 100%;
        z-index: 1;
    }
    &.banner-innerpage-content {
        text-transform: inherit;
    }

    &.banner-innerpage-content .inside {
        position: relative;
        display: table-cell;
        z-index: 2;
        vertical-align: middle;
    }
}

.banner-innerpage-link {
    display: inline-block;
    width: 100%;
}

.banner {
    background: $secondary-color;
    color: $white-color;
    padding: 40px 0;
    margin: 80px 0;

    h2 {
        margin: 20px 0;
    }

    &.widget-static-content {
        position: relative;
        text-transform: inherit;
    }
    &.widget-static-content .static-content-cover {
        background-position: center center;
        background-repeat: no-repeat;
        background-size: 100% auto;
        height: 100%;
        position: absolute;
        top: 0;
        width: 100%;
        z-index: 1;

        @include opacity(0.3);

    }
    &.widget-static-content {
        text-transform: inherit;
    }

    &.widget-static-content .inside {
        position: relative;
        z-index: 2;
    }
}

/************************************************************************************
Footer
*************************************************************************************/

.pre-footer {
    padding-bottom: 10px;

    .delivery {
        text-align: right;
    }

    .list-inline > li {
        padding-left: 0;
        margin-bottom: 4px;
    }
}

footer {
    .footer {
        background: $primary-color;
        color: rgba( $white-color, .4 );
    }
    .footer-nav {

        padding: 50px 0 40px;

        p {
            margin: 0;
            padding: 0;
        }

        nav {

            a {
                color: rgba( $white-color, .4 );
                &:hover {
                    color: $secondary-color;
                }
            }
            .active a {
                color: $secondary-color;
            }
        }
        .copy {
            color: rgba( $white-color, .4 );
        }

        .fiscal {
            max-height: 60px;
        }
    }

    .sub-footer {
        background-color: $primary-color-dark;
        color: rgba( $white-color, .2 );
        padding: 10px 0 4px;

        p {
            color: $primary-color-light;
        }
        a {
            color: rgba( $white-color, .4 );
                text-decoration: underline;
            &:hover {
                color: $secondary-color;
            }

        }
        .social {
            text-align: right;

            .list-inline > li {
                padding-left: 6px;
                padding-right: 6px;
            }
            .fa-inverse {
                color: $primary-color-dark;
            }
        }
    }   
}

.afip,
.ebit {
    text-align: right;
    vertical-align: middle;
    max-width: 70px;
}
.siteforte {
    text-align: right;
    vertical-align: middle;
    max-width: 100px;
}

.afip img,
.ebit img,
.siteforte img {
    display: block;
    float: right;
    width: 100%;
    height: auto;
    max-height: 80px;
}

/************************************************************************************
Buttons
*************************************************************************************/
.btn {

    border-radius: 0;
    border: none;

    &.btn-default {
        background-color: $white-color;
        border-color: $black-color-lighter;
        color: $black-color;
    }

    &.btn-default:hover, &.btn-default:focus, &.btn-default:active, &.btn-default.active {
        background-color: $black-color-light;
    }

    &.btn-primary {
        background-color: $secondary-color;
        color: $white-color;
    }

    &.btn-secundary {
        background-color: $primary-color;
        color: $white-color;
    }

    &.btn-primary:hover, &.btn-primary:focus, &.btn-primary:active, &.btn-primary.active {
        opacity: .85;

    }
     &.btn-secundary:hover, &.btn-secundary:focus, &.btn-secundary:active, &.btn-secundary.active {
        opacity: .85;
    }

    &.btn-search {
        padding: 6px 8px;
        background: $white-color;
        color: $primary-color;
        border: 1px solid $white-color;
        &:hover {
            color: $secondary-color;
        }
    }

    &.btn-lg {
    border-radius: 0;
    font-size: 14px;
    font-weight: bold;
    line-height: 1.33;
    padding: 16px 24px;
    }

    &.btn-sm {
        border-radius: 0;
        font-size: 12px;
        line-height: 1.5;
        padding: 5px 10px;
    }

    &.btn-xs {
        border-radius: 0;
        font-size: 12px;
        line-height: 1.5;
        padding: 1px 5px;
    }

    &.btn-transparent {
        background-color: $white-color;
        border-color: $white-color;
        color: $primary-color !important;
    }

    &.btn-transparent:hover, &.btn-transparent:focus, &.btn-transparent:active, &.btn-transparent.active {
        background-color: $black-color-softer;
        color: $secondary-color;
    }

    &.cart {
    }

    &.dropdown-toggle {
        text-align: left;
    }

    &.dropdown-toggle {
        border: 1px solid $black-color-soft;
    }

    &.nostock {
        cursor: not-allowed;
        pointer-events: none;
        opacity: .65;
        filter: alpha(opacity=65);
        -webkit-box-shadow: none;
        box-shadow: none;
    }
}

/************************************************************************************
SOCIAL ICONS
*************************************************************************************/
.facebook {

    &:before {
        content: '\f09a';
        font-family: FontAwesome;
        display: inline-block;
        font-size: 16px;
    }
}
.twitter {

    &:before {
        content: '\f099';
        font-family: FontAwesome;
        display: inline-block;
        font-size: 16px;
    }
}
.google_plus {

    &:before {
        content: '\f0d5';
        font-family: FontAwesome;
        display: inline-block;
        font-size: 16px;
    }
}
.pinterest {

    &:before {
        content: '\f0d2';
        font-family: FontAwesome;
        display: inline-block;
        font-size: 16px;
    }
}
.instagram {

    &:before {
        content: '\f16d';
        font-family: FontAwesome;
        display: inline-block;
        font-size: 16px;
    }
}

.page-contact .facebook:before {
    margin-right: 19px;
}
/************************************************************************************
ICON FONT
*************************************************************************************/
i {
    font-family: FontAwesome !important;
}

/************************************************************************************
DIVIDER
*************************************************************************************/
.divider, .featurette-divider {
    margin-top: 20px;
    border-top: 1px solid;
}
.divider {
    border-color:$white-color;
}

.featurette-divider {
    border-color:$black-color-soft;
}

/************************************************************************************
PRICE
*************************************************************************************/

#price_display {
    color: $secondary-color;
    h2 {
        margin-top: 6px !important;
    }
}
#compare_price_display {
    color: $black-color-lighter;
    text-decoration:line-through;
    font-weight: normal;

    h4 {
        font-weight: normal;
        margin: 0;
    }
}

/************************************************************************************
QUICK SHOP
*************************************************************************************/
.descriptioncolContent {
    .title {
        h2 {
            margin-top: 0;
        }
    }
}

/************************************************************************************
Superfish
*************************************************************************************/

/*** ESSENTIAL STYLES ***/

.sf-menu, .sf-menu * {
    margin:         0;
    padding:        0;
    list-style:     none;
}
.sf-menu {
    line-height:    1.0;
}
.sf-menu ul {
    position:       absolute;
    top:            -999em;
    width:          10em; /* left offset of submenus need to match (see below) */
}
.sf-menu ul li {
    width:          100%;
    text-align: left;
}
.sf-menu li:hover {
    visibility:     inherit; /* fixes IE7 'sticky bug' */
}
.sf-menu li {
    position:       relative;
}
.sf-menu a {
    display:        block;
    position:       relative;
}
.sf-menu li:hover ul,
.sf-menu li.sfHover ul {
    left:           0;
    top:            48px; /* match top ul list item height */
    z-index:        99999;
}
ul.sf-menu li:hover li ul,
ul.sf-menu li.sfHover li ul {
    top:            -999em;
}
ul.sf-menu li li:hover ul,
ul.sf-menu li li.sfHover ul {
    left:           10em; /* match ul width */
    top:            -1px;
}
ul.sf-menu > li > a > .arrow{
    display:none;
}

/*** DEMO SKIN ***/
#navigation {
    width:100%;
}

.sf-menu {
    width: 100%;
    text-align: right;
}
.sf-menu a {
    font-size: 14px;
    font-weight: normal;
    padding:10px 10px 30px 10px;
    letter-spacing: -0.5;
    text-decoration:none;
    color: $primary-color;
     box-shadow: none;
     -moz-box-shadow: none;
    -webkit-box-shadow: none;
}

.sf-menu a:hover,
.sf-menu li.active a,
.sf-menu li:hover > a {
    color: $secondary-color;
    box-shadow:  inset 0 -5px 0 0 $secondary-color;
    -moz-box-shadow:   inset 0 -5px 0 0 $secondary-color;
    -webkit-box-shadow: inset 0 -5px 0 0 $secondary-color;
}    
 
.sf-menu ul {
   background: rgba($white-color,.9);
    border: 1px solid $black-color-soft;
    margin-top: 6px;
}
.sf-menu ul a {
    padding:12px 6px;
    background: transparent;

}
.sf-menu ul a:hover {
    background: $secondary-color;
    color: $white-color !important;
     box-shadow: none;
     -moz-box-shadow: none;
    -webkit-box-shadow: none;
}

.sf-menu li.active ul a {
   background: rgba($white-color,.9);
    color: $primary-color;
     box-shadow: none;
     -moz-box-shadow: none;
    -webkit-box-shadow: none;
}
.sf-menu li.active ul a:hover {
    background: $secondary-color;
    color: $white-color;
}

.sf-menu li.first a{
    padding-left:20px;
}
.sf-menu li {
    // background: $white-color;
    display: inline-block;
}

.sf-menu li:hover, .sf-menu li.sfHover,
.sf-menu a:focus, .sf-menu a:hover, .sf-menu a:active {
    outline: 0;
}

.sf-menu ul li ul li a, .sf-menu ul li ul li ul li a{
    text-align: left;
}

.sf-menu li.selected a {
    text-align: left;
}

.sf-menu li.selected ul li a {
    border: none;
}
.sf-menu i {
    font-size: 10px;
    margin-left: 6px;
}

.sf-menu li.sfHover ul {
    margin-top: 0;
}

.dropdown-menu {
    max-height: 600px;
    overflow-y: auto;
}


// .mobile navigation
.mobile {
    font-size: 16px;
    margin-left: 20px;
     padding: 0 0 10px;

    & a {
        color: $primary-color;
        &:hover {
            background: $black-color-soft !important;
        }
    }

    & .dropdown-menu > .active > ul,
    & .dropdown-menu ul {
        list-style: none;
        padding-left: 0;
        
    }

     .dropdown-menu > .active > ul > li,
     .dropdown-menu ul li {
        overflow: hidden;
        padding: 3px 5px 6px 20px;

        & a {
            color: $primary-color;
        }
     }

    .dropdown-menu > .active > a,
    .dropdown-menu > .active > a:hover, 
    .dropdown-menu > .active > a:focus {
        color: $white-color !important;
        background: $secondary-color;
        font-variant: bold !important;
    }

    .dropdown-menu > .active > ul > .active {
        background: $black-color-lighter !important;
        color: $white-color !important;
    }

    .dropdown-menu .active ul li:hover,
    .dropdown-menu ul li:hover {
        color: $primary-color !important;
        background: $black-color-soft !important;

    }
}

/************************************************************************************
MEDIA QUERIES
*************************************************************************************/
@media (max-width: 320px) {

    .header-message {
        margin: 0;
        margin-bottom: -5px;
    }
    .languages {
        text-align: left !important;
    }

    .title-wrapper {
        .title {
            margin-right: 0;
            min-width: 100%;

        }
        .price-holder {
            min-width: 100%;
            margin-right: 0;
            float: left;
            text-align: left;
            min-width: 100% !important;

            .price {
                text-align: left;
            }
        }
    }
    .page-product {
        .installments {
            text-align: left;
        }
    }

    .col-xxs {

        &.last {
            content: ' ';
            width: 5% !important;
            margin: 0 !important;
        }
        &.col-delete {
            text-align: right !important;
            width: 4% !important;
            margin-right: 0 !important;
            margin-left: 1% !important;
            margin-top: 35px !important;
        }
        &.col-quantity {
            text-align: right !important;
            width: 20% !important;
            margin-right: 5% !important;
        }

    }

}

@media (max-width: 480px) {

    .col-xxs {
        display: inline-block !important;
        width: 20% !important;
        margin-left: 0;
        margin-right: 0;
        text-align: left !important;
        padding: 0 !important;

        &.last {
            content: ' ';
        }
        &.col-delete {
            text-align: right !important;
            width: 4% !important;
            margin-right: 6%;
        }
        &.col-quantity {
            text-align: right !important;
            width: 15% !important;
            margin-right: 10% !important;
        }
        &.col-product {
            width: 25% !important;
        }
    }
}

@media (max-width: 600px) {

    .page-product {
        .installments {
            text-align: left;
        }
    }

    .footer-nav {
        .copy {
             text-align: center !important;
        }
        ul.list-inline {
            display: block;
            position: static;
            float: none !important;
            text-align: center !important;

            &.pull-right {
                float: none !important;
            }
        }
        nav {
            margin: 40px 0;
            .list-inline > li {
                display: block !important;
                margin-left: 0;
                margin: 6px 0;

            }
        }
    }

    .banner-copy {
        font-size: 1.33em;
    }
}

@media (max-width: 767px) {

// .visible-xs 

    .navbar > .container .navbar-brand {
        margin-left: 15px;
    }
    .navbar-nav > li > a {
        border-bottom: 1px solid $black-color-soft;
        margin: 0;
    }
    .navbar-nav > li > a:hover,
    .navbar-default .navbar-nav > .active > a, 
    .navbar-default .navbar-nav > .active > a:hover, 
    .navbar-default .navbar-nav > .active > a:focus {
        background-color: $secondary-color !important;
        color: $white-color !important;
        border-bottom: 1px solid $secondary-color-dark;
        margin-top: 1px;
    }
    .navbar-default .navbar-nav > .open {
        border-bottom: 0;
        margin-bottom: 0;
    }

    .message-wrapper,
    .account-wrapper,
    .languages-wrapper,
    .search-content-wrapper {
        float: none;
        position: static;
        text-align: left;
        margin-left: 0;
    }

    .search-content-wrapper {
        width: 100% !important;
    }
    .languages-wrapper {
        margin-left: 10 !important;
        width: 18%;
    }
    .account-wrapper {
        float: left !important;
        width: 80% !important;
        margin-left: 0 !important;
        #auth {
            text-align: left !important;
        }
    }

    .top-header-rightcolumn {

        position: static;
        width: 100% !important;
    }

    .navbar-nav > li > a,
    .navbar-brand,
    #cart {
        padding-top: 5px;

    }
    .affix-top {
        padding-top: 31px;
        padding-bottom: 10px;
    }
    .affix {
        & #logo {
            margin-top: 5px;

        }
    }
    .cloud-zoom-wrap {
        #zoom img {
            min-width: 100%;
        }
        .mousetrap {
            display: none;
            visibility: hidden;
        }
    }
    #shoppingCartPage {

        .cart-contents {

            .firstrow {
                font-size: 14px;

            }
            .productrow {

                li {
                    text-align: right;
                }
                
                .name {
                   margin: 5px 0 0 0;
                   text-align: center;
                }
                a {
                    font-size: 12px;
                    line-height: 110%;
                }
                .col-price {
                    font-size: 14px;
                }
                .col-subtotal {
                    font-size: 14px;
                }
            }
        } 
    } 
}

@media (min-width: 768px) and (max-width: 991px) {

// .visible-sm

    .cloud-zoom-wrap {
        #zoom img {
            min-width: 100%;
        }
        .mousetrap {
            display: none;
            visibility: hidden;
        }
    }

    .top-header {
        .container {
            position: relative;
        }
    }

    .message-wrapper,
    .account-wrapper,
    .languages-wrapper,
    .search-content-wrapper {
        float: none;
        position: static;
        text-align: left;
        margin-left: 0;
    }

    .search-content-wrapper {
        width: 100% !important;
    }
    .languages-wrapper {
        display: inline-block !important;
        margin-left: 15px !important;
    }
    .account-wrapper {
        float: none !important;
        width: 280px;
    }

    .top-header-rightcolumn {
        display: block;
        position: absolute;
        right: 15px;
    }

    #compare_price_display {
        font-size: 18px !important;
    }
    .title-wrapper {
        .price-holder {
            font-size: 26px !important;
        }        
    }

    .widgets {
        .widget {
            margin-bottom: 40px;
        }
    }
     .navbar-nav {
        float: right;
    }
    .navbar-nav > li > a,
    .navbar-brand,
    #cart {
        padding-top: 5px;

    }
    .affix-top {
        padding-top: 31px;
        padding-bottom: 10px;
    }
    .affix {
        & #logo {
            margin-top: 5px;

        }
    }

    .col-xxs {
        display: inline-block !important;
        width: 14% !important;
        margin-left: 0;
        margin-right: 0;
        text-align: left !important;
        padding: 0 !important;

        &.col-quantity {
            width: 20% !important;
        }

        &.last {
            content: ' ';
        }
        &.col-delete {
            text-align: right !important;
            width: 2% !important;
            margin-right: 1%;
        }
        &.col-quantity {
            text-align: right !important;
            width: 10% !important;
            margin-right: 5% !important;
        }
        &.col-image-product {
            width: 55% !important;
        }
        &.col-product {
            width: 40% !important;
        }
    }

    #shoppingCartPage {
        .cart-contents {

            .firstrow {

            }
            .productrow {

                li {
                    text-align: right;
                    font-size: 18px;
                }
                .name {
                    font-size: 13px !important;
                }
            } 
        } 
    } 
}

@media (min-width: 992px) and (max-width: 1199px) {

  // .visible-md 
}

@media (min-width: 1200px) {

  // .visible-lg

    .cloud-zoom-wrap {
        #zoom img {
            max-width: 100%;
        }
        .mousetrap {
            display: block;
            visibility: visible;
        }
    }
}


/************************************************************************************
TWITTER
*************************************************************************************/
iframe .timeline-header,
iframe .timeline-footer {
    display: none !important;
    visibility: hidden !important;
}
.customisable-border {
    border: none;
}
.tweet {
    border-width: 0 0 1px;
    padding: 12px 12px 10px 0 !important;
}

/************************************************************************************
Facebook
*************************************************************************************/

._8r {
    display: none !important;
    visibility: hidden !important;
}

.uiList {
    padding: 10px;
    padding-top: 40px !important;
}

.fb-comments,
.fb-comments iframe[style],
.fb-comments .pluginSkinLight > div,
.fb-comments > span,
.fb-comments #feedback_06PwWERA4ObWH1fn3 {
    width: 100% !important;
}


/* Make the Facebook Like box responsive (fluid width)
https://developers.facebook.com/docs/reference/plugins/like-box/ */

/* This element holds injected scripts inside iframes that in 
some cases may stretch layouts. So, we're just hiding it. */

#fb-root {
  display: none;
}

/* To fill the container and nothing else */

.fb_iframe_widget, .fb_iframe_widget span, .fb_iframe_widget span iframe[style] {
  width: 100% !important;
}




/************************************************************************************
CUSTOM CSS
*************************************************************************************/
::selection{
    background: $secondary-color;
}
::-moz-selection {
    background: $secondary-color;
}
h1, h2, h3, h4 {
	font-family: $fonthead!important;
}
.btn {
	font-family: $fontbut!important;
}

.top-header,
.btn.btn-secundary,
footer .footer,
.thumbnail .out-of-stock
{
    background: {{ settings.primary_color }};
}

.form-control:focus,
.page-contact .form-control:focus {
    border-left: 2px solid $secondary-color!important;
}

a, active, a:hover, a:focus,
.top-header a:hover, 
#menu .sf-menu a,
.btn.btn-search:hover,
.welcomeMessage,
.sf-menu a:hover,
.sf-menu li.active a,
.sf-menu li:hover > a,
.product-description-title,
.price,
#breadcrumb .crumb:hover,
#breadcrumb .crumb.last,
#shoppingCartPage .cart-contents .productrow .col-delete a:hover:before,
#shoppingCartPage .cart-contents .productrow .name:hover,
.navbar .cart-summary a:hover,
.navbar-default .navbar-toggle:hover, 
.navbar-default .navbar-toggle:focus,
.modal-dialog .modal-title,
.page-contact ul li:hover > a,
footer nav a:hover,
footer nav .active a,
footer .sub-footer a:hover,
.btn-search:hover, .btn-transparent:hover, .btn-transparent:focus, .btn-transparent:active, .btn-transparent.active,
#price_display,
.product-description-wrap .product-description-title,
footer .footer-nav nav a:hover,
.page-product .installments #installments_number, .page-product .installments #installments_amount,
.page-contact *:hover:before
{
	color: $secondary-color;
}

.sf-menu ul a:hover,
.image-wrap,
.btn.btn-primary,
.thumbnail .offer,
.navbar .cart-summary .badge,
.mobile .dropdown-menu > .active > a,
.mobile .dropdown-menu > .active > a:hover, 
.mobile .dropdown-menu > .active > a:focus,
.sf-menu li.active ul a:hover,
.banner
{
	background: $secondary-color;
}

#no-logo,
.btn.btn-search,
.sf-menu a,
.sf-menu li.active ul a,
.caption,
#shoppingCartPage .cart-contents .productrow .name,
.total-price,
.navbar .fa-shopping-cart,
.navbar .cart-summary a,
#shoppingCartPage .cart-contents .productrow .col-delete a:before,
#shoppingCartPage .cart-contents .total-price,
.fa-shopping-cart,
.page-contact ul li, .page-contact ul li a,
btn.btn-search,
.mobile .dropdown-menu ul li a,
.mobile .dropdown-menu > .active > ul > li a, 
.mobile .dropdown-menu ul li a,
.thumbnail .caption .title a,
.page-contact *:before,
.form-control:-moz-placeholder,
.form-control::-moz-placeholder,
.form-control:-ms-input-placeholder,
.form-control::-webkit-input-placeholder
{
	color: {{ settings.primary_color }};
}

#main-menu,
#menu .sf-menu a,
.footer-nav,
.powered-by {
	font-family: $fontnav;
}

.btn {
	font-family: $fontbut;
}

.sf-menu a:hover,
.sf-menu li.active a,
.sf-menu li:hover > a,
.sf-menu a:hover, .sf-menu li.active a, .sf-menu li:hover > a
 {
	 box-shadow:  inset 0 -5px 0 0 $secondary-color;
	-moz-box-shadow:   inset 0 -5px 0 0 $secondary-color;
	-webkit-box-shadow: inset 0 -5px 0 0 $secondary-color;
} 

.widget-static-content .static-content-cover {
 	background-image: url( "{{ "banner-home.jpg" | static_url }}" );
}

#banner-innerpage.banner-innerpage-content.contact-banner .banner-innerpage-cover {
 	background-image: url( "{{ "banner-contact.jpg" | static_url }}" );
}
#banner-innerpage.banner-innerpage-content.category-banner .banner-innerpage-cover {
 	background-image: url( "{{ "banner-products.jpg" | static_url }}" );
}
#banner-innerpage.category-banner .banner-copy {
	color: $banner-category-color;
}
#banner-innerpage.contact-banner .banner-copy {
	color: $banner-contact-color;
}

//Infinite scrolling
.infinite-scroll-loading {
    background: url( "{{ "img/loading.gif" | static_url }}" ) no-repeat center;
    display: block;
    height: 80px;
    width: inherit;
    overflow: hidden;
}

/************************************************************************************
Account
*************************************************************************************/

.page-account {
    .help-block {
        margin-top:20px;
    }
    .bg-primary, .bg-success, .bg-info, .bg-warning, .bg-danger {
        padding: 15px;
        margin: 10px 0;
    }
    .addresses {
        clear: left;

        li {
            margin: 10px 0;
        }
    }

    .new-address {
        margin-top: 10px;
        clear: left;
    }

    .order-box img {
        vertical-align: middle;
        margin-right: 4px;
        max-width: 50px;
        max-height: 50px;
    }

    .order-actions{
        margin:10px 0;
    }
}

/**
* BxSlider v4.0 - Fully loaded, responsive content slider
* http://bxslider.com
*
* Written by: Steven Wanderski, 2012
* http://stevenwanderski.com
* (while drinking Belgian ales and listening to jazz)
*
* CEO and founder of bxCreative, LTD
* http://bxcreative.com
*/


/** RESET AND LAYOUT
===================================*/

.bx-wrapper {
    position: relative;
    margin: 0 auto 60px;
    padding: 0;
    *zoom: 1;
}

.bx-wrapper img {
    max-width: 100%;
    display: block;
}

/** THEME
===================================*/

.bx-wrapper .bx-viewport {
    -moz-box-shadow: 0 0 5px $secondary-color;
    -webkit-box-shadow: 0 0 5px $secondary-color;
    background: $white-color;
}

.bx-viewport {
    max-height: 500px !important;
}

.bx-wrapper .bx-pager,
.bx-wrapper .bx-controls-auto {
    position: absolute;
    bottom: -30px;
    width: 100%;
}

/* LOADER */

.bx-wrapper .bx-loading {
    min-height: 50px;
    background: url({{ 'img/loader-1.gif' | static_url }}) center center no-repeat $primary-color;
    height: 100%;
    width: 100%;
    position: absolute;
    top: 0;
    left: 0;
    z-index: 2000;
}

/* PAGER */

.bx-wrapper .bx-pager {
    text-align: center;
    font-size: .85em;
    font-family: Arial;
    font-weight: bold;
    color: $primary-color-light;
    padding-top: 20px;
}

.bx-wrapper .bx-pager .bx-pager-item,
.bx-wrapper .bx-controls-auto .bx-controls-auto-item {
    display: inline-block;
    *zoom: 1;
    *display: inline;
}

.bx-wrapper .bx-pager.bx-default-pager a {
    background: $primary-color-light;
    text-indent: -9999px;
    display: block;
    width: 10px;
    height: 10px;
    margin: 0 5px;
    outline: 0;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
}

.bx-wrapper .bx-pager.bx-default-pager a:hover,
.bx-wrapper .bx-pager.bx-default-pager a.active {
    background: $primary-color;
}

/* DIRECTION CONTROLS (NEXT / PREV) */

.bx-wrapper .bx-prev {
    left: 10px;
    background: url({{ 'js/bxslider/images/controls.png' | static_url }}) no-repeat 0 -32px;
}

.bx-wrapper .bx-next {
    right: 10px;
    background: url({{ 'js/bxslider/images/controls.png' | static_url }}) no-repeat -43px -32px;
}

.bx-wrapper .bx-prev:hover {
    background-position: 0 0;
}

.bx-wrapper .bx-next:hover {
    background-position: -43px 0;
}

.bx-wrapper .bx-controls-direction a {
    position: absolute;
    top: 50%;
    margin-top: -16px;
    outline: 0;
    width: 32px;
    height: 32px;
    text-indent: -9999px;
    z-index: 9999;
}

.bx-wrapper .bx-controls-direction a.disabled {
display: none;
}

/* AUTO CONTROLS (START / STOP) */

.bx-wrapper .bx-controls-auto {
    text-align: center;
}

.bx-wrapper .bx-controls-auto .bx-start {
    display: block;
    text-indent: -9999px;
    width: 10px;
    height: 11px;
    outline: 0;
    background: url({{ 'js/bxslider/images/controls.png' | static_url }}) -86px -11px no-repeat;
    margin: 0 3px;
}

.bx-wrapper .bx-controls-auto .bx-start:hover,
.bx-wrapper .bx-controls-auto .bx-start.active {
    background-position: -86px 0;
}

.bx-wrapper .bx-controls-auto .bx-stop {
    display: block;
    text-indent: -9999px;
    width: 9px;
    height: 11px;
    outline: 0;
    background: url({{ 'js/bxslider/images/controls.png' | static_url }}) -86px -44px no-repeat;
    margin: 0 3px;
}

.bx-wrapper .bx-controls-auto .bx-stop:hover,
.bx-wrapper .bx-controls-auto .bx-stop.active {
    background-position: -86px -33px;
}

/* PAGER WITH AUTO-CONTROLS HYBRID LAYOUT */

.bx-wrapper .bx-controls.bx-has-controls-auto.bx-has-pager .bx-pager {
    text-align: left;
    width: 80%;
}

.bx-wrapper .bx-controls.bx-has-controls-auto.bx-has-pager .bx-controls-auto {
    right: 0;
    width: 35px;
}

/* IMAGE CAPTIONS */

.bx-wrapper .bx-caption {
    position: absolute;
    bottom: 0;
    left: 0;
    background: $primary-color-light;
    background: rgba(80, 80, 80, 0.75);
    width: 100%;
}

.bx-wrapper .bx-caption span {
    color: $primary-color;
    font-family: Arial;
    display: block;
    font-size: .85em;
    padding: 10px;
}

























/* CUSTOM */
#wrap {
    display: block;
    width: 100%;
    padding: 0px;
    margin: 50px auto 0px auto;
    position: relative;
}







#navigation {
    background: #FFF !important;
    color: #FFF;
    height: 45px !important;
    padding: 0px !important;
    margin: 0px !important;
    border-bottom: solid 1px #CCC;
    box-shadow: 0px 0px 15px rgba(0,0,0,0.2);
    -webkit-box-shadow: 0px 0px 15px rgba(0,0,0,0.2);
    -moz-box-shadow: 0px 0px 15px rgba(0,0,0,0.2);
    position: fixed !important;
    top: 0px !important;
    left: 0px;
}
#logo {
    margin: 10px 0px 0px 0px !important;
}

.message-wrapper {
    float: left;
}

#accountWrap {
    
    font-size: 12px !important;
    color: #FFF;
    
    line-height: 50px;
    
    height: 50px;
    
    margin: 0px 10px;
    
    float: right;
}
#accountWrap .list-inline {
    padding: 0px;
    margin: 0px;
}


#cart {
    background: url( "{{ "img/icon-cart.png" | static_url }}" ) no-repeat center transparent;
    width: 44px;
    height: 50px;
    padding: 0px;
    margin: 0px;
    float: right;
}
#cart a {
    display: block;
    width: 44px;
    height: 50px;
    padding: 0px;
    margin: 0px;
}
#cart .badge {
    background: #959595 !important;
    font-size: 12px !important;
    color: #FFF !important;
    padding: 1px 2px !important;
    margin: 10px 0px 0px 0px !important;
    border-radius: 0px !important;
    position: relative;
    top: 13px;
    left: -16px;
}
#searchWrap {
    float: right;
    width: 110px !important;
    height: 50px;
    margin: 0px 10px;
    position: relative;
}
#searchWrap form {
    margin: 8px 0px 0px 0px;
}
#searchWrap form .text-input {
    background: #F5F5F5;
    font-style: italic;
    color: #959595 !important;
    width: 110px;
    padding:  0px 50px 0px 10px;
    border-top: solid 1px #959595;
    border-right: none;
    border-bottom: solid 1px #959595;
    border-left: solid 1px #959595;
    position: absolute;
    right: -110px;
    z-index: 1;
}
#searchWrap form .text-input:focus {
    width: 250px;
    border-top: solid 1px #959595;
    border-right: none;
    border-bottom: solid 1px #959595;
    border-left: solid 1px #959595;
}
#searchWrap form .btn-search {
    background: url( "{{ "img/icon-search.png" | static_url }}" ) no-repeat center #FFF;
    display: block;
    width: 40px;
    height: 34px;
    border-top: solid 1px #959595;
    border-right: solid 1px #959595;
    border-bottom: solid 1px #959595;
    border-left: none;
    position: absolute;
    right: -110px;
    z-index: 2;
}
.full-menu {
    background-color: #FFF;
    line-height: 32px;
    width: 100%;
    height: 33px;
    border-bottom: solid 1px #CCC;
}
.full-menu.padding-top {
    
}
#menuFull,
#menuFull li {
    
    height: 35px;
    padding: 0px;
    margin: 0px;
    list-style: none;
    
}
#menuFull {
    text-align: center;
}
#menuFull li {
    font-size: 15px;
    text-transform: lowercase;
    display: inline-block;
    position: relative;
}
#menuFull li .menu-arrow-down {
    
}
#menuFull li a {
    color: #999;
    display: block;
    height: 33px;
    padding: 0px 10px;
    border-bottom: solid 1px #CCC;
}
#menuFull li a:hover {
    background: #FFF;
    border-bottom: solid 1px #666;
}
#menuFull li.active a {
    background: #FFF;
    border-bottom: solid 1px #666;
}
#menuFull .full-menu-item .full-menu-sub {
    text-align: left !important;
    display: none;
    padding: 0px;
    min-width: 150px;
    max-width: 300px;
    margin: 0px;
    position: absolute;
    top: 32px;
    left: 0px;
    z-index: 99999999 !important;
}

#menuFull .full-menu-item .full-menu-sub .full-menu-item  {
    background: #FFF;
    display: block;
    border-top: none;
    border-right: solid 1px #CCC;
    border-bottom: solid 1px #CCC;
    border-left: solid 1px #CCC;
}
#menuFull .full-menu-item .full-menu-sub .full-menu-item:hover,
#menuFull .full-menu-item .full-menu-sub .full-menu-item.active {
    border-left: solid 1px #666;
}
#menuFull .full-menu-item .full-menu-sub .full-menu-item a,
#menuFull .full-menu-item .full-menu-sub .full-menu-item a:hover {
    border: none !important;
}
#menuFull .full-menu-item .full-menu-sub li {
    padding: 0px;
    margin: 0px;
}

.thumbnail {
    background: #FFF !important;
}


.full-banner,
.full-banner-wrap,
.full-banner-item,
.full-banner-link,
.full-banner-img {
    padding: 0px;
    margin: 0px;
}

.full-banner {
    background: url( "{{ "img/loader-2.gif" | static_url }}" ) no-repeat center #222;
    display: block;
    width: 100%;
    height: 300px;
    padding: 0px;
    margin: 0px;
    position: relative;
    overflow: hidden;
}
.full-banner-wrap {
    width: 100%;
    height: 300px;
    overflow: hidden;
}
.full-banner-item {
    
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-position: top center;
    background-size: auto;
    width: 100%;
    height: 300px;
    
    position: absolute;
    
}
.full-banner-link {
    
}
.full-banner-img {
    
}


.full-banner.half-height {
    height: 70px !important;
}
.full-banner.half-height .full-banner-wrap {
    height: 70px !important;
}
.full-banner.half-height .full-banner-item {
    background-image: url( "{{ "img/banner-product-temp.png" | static_url }}" );
    background-size: 100% !important;
    height: 70px !important;
}

.thumbnail .caption,
.thumbnail .caption .title,
.thumbnail .caption .row,
.thumbnail .caption .row .price  {
    padding: 0px !important;
    margin: 0px !important;
}


.thumbnail .caption {
    height: 70px;
    padding: 5px 0px 0px 0px !important;
    position: relative;
}

.thumbnail .caption .title {
    font-family: 'Helvetica' !important;
    font-size: 15px;
    color: #828282;
    text-transform: lowercase;
    line-height: 16px;
    letter-spacing: -0.9px;
    width: 90%;
    height: 20px;
    float: left;
}

.thumbnail .caption .row  {
    font-family: 'Helvetica' !important;
    font-style: normal;
    font-weight: normal;
    font-size: 18px;
    padding: 0px !important;
    float: right;
    
    position: absolute;
    top: 3px;
    right: 0px;
}

.thumbnail .caption .row .price  {
    color: #4b4b4b !important;
    display: block;
}




/* PRDUCT */

.product-title {
    font-size: 20px;
    color: #FFF;
    text-transform: uppercase;
    text-shadow: 2px 2px 0px #000;
    line-height: 30px;
    height: 30px;
    padding: 0px 15px;
    margin: 0px 0px 20px 0px;
    border-bottom: solid 2px #000;
}
.product-title span {
    background: #555;
    display: inline-block;
    height: 30px;
    padding: 0px 10px;
}



.productContainer .col-md-7 {
    padding-right: 0px !important;
}
.productContainer .col-md-5 {
    padding: 0px !important;
}
.product-single {
    
}
.product-single .products-header {
    height: 25px;
    margin: 20px 0px 0px 0px;
}
.descriptioncolContent .title-wrapper {
    padding: 0px;
    margin: 0px 0px 10px 0px !important;
}
.descriptioncolContent .title-wrapper .title h2 {
    text-transform: lowercase;
    margin: 0px 0px 0px 0px !important;
}
.descriptioncolContent .fancyContainer {
    padding: 10px 0px 25px 0px;
}
.descriptioncolContent .price-holder {
    background: #d4d4d4;
    font-size: 33px;
    color: #FFF !important;
    padding: 5px 15px;
    margin: 0px 0px;
    float: left;
}
.descriptioncolContent .addToCartButton {
    float: right;
}
.descriptioncolContent .addToCartButton input {
    background: #000 !important;
    font-size: 20px;
    text-transform: uppercase !important;
    
    color: #FFF !important;
}

.social-container-box {
    margin: 20px 0px 0px 0px;
}

.product-description-wrap {
    margin: 10px 0px 50px 0px !important;
}
.product-description-title {
    text-transform: uppercase !important;
    padding: 5px 0px 5px 0px;
    border-bottom: solid 2px #333;
}

.picture-product {
    
}
.picture-product .social-container-box {
    text-align: right;
    padding: 0px;
    margin: 5px 0px 0px 0px;
    
    
}
.picture-product {
    
}


.products-list {
    
    
    
    
}

.footer {
    background: #f3f3f3 !important;
    color: #CCC;
    border-top: solid 1px #666 !important;
}



#breadcrumb {
    font-size: 13px;
    font-style: italic;
    color: #969696 !important;
    letter-spacing: -0.5px;
    
}

#breadcrumb .crumb {
    
    color: #818181 !important;
    
}





.clear {
    clear: both;
}



