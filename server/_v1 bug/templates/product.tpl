

<!--<div class="container full-banner half-height">
    <ul class="full-banner-wrap">
        <li class="full-banner-item"></li>
    </ul>
</div>-->

<div class="container full-menu padding-top">
    <ul id="menuFull" class="">
        {% snipplet "navigation.tpl" %}
    </ul>
</div>
<div class="container product-single">
    <div class="products-header">
        <div class="breadcrumbs-wrapper">{% snipplet "breadcrumbs.tpl" %}</div>
        <div class="title"><h2>{{ category.name }}</h2></div>
    </div>
</div>


<div class="page-product container page">
    <div class="row">
        
        <div id="single-product" itemscope itemtype="http://schema.org/Product">
            <div class="productContainer" data-variants="{{ product.variants_object | json_encode }}">
                <div class="col-md-7">
                    <div class="picture-product row imagecol">
                        <div class="col-md-7">
                            <div class="imagecolContent">
                                {% if product.featured_image %}
                                    <a href="{{ product.featured_image | product_image_url('original') }}" id="zoom"
                                       class="cloud-zoom"
                                       rel="position: 'inside', showTitle: false, loading: '{{ 'Cargando...' | translate }}'">
                                        {{ product.featured_image | product_image_url('large') | img_tag }}
                                    </a>
                                {% else %}
                                    {{ product.featured_image | product_image_url('large') | img_tag }}
                                {% endif %}
                            </div>
                            <div class="col-md-12">
                                {% if product.images_count > 1 %}
                                    <div id="tS3" class="jThumbnailScroller">
                                        <div class="jTscrollerContainer">
                                            <div class="jTscroller">
                                                {% for image in product.images %}
                                                    <a href="{{ image | product_image_url('original') }}"
                                                       class="cloud-zoom-gallery"
                                                       rel="useZoom: 'zoom', smallImage: '{{ image | product_image_url('large') }}' ">
                                                        {{ image | product_image_url('thumb') | img_tag(product.name) }}
                                                    </a>
                                                {% endfor %}
                                            </div>
                                        </div>
                                        <a href="#" class="jTscrollerPrevButton"></a>
                                        <a href="#" class="jTscrollerNextButton"></a>
                                    </div>
                                {% endif %}
                            </div>
                            <div class="social-container-box col-md-10">
                                <ul class="shareLinks list-inline">
                                    <li class="shareItem facebook">
                                        {{ product.social_url | fb_like('store-product', 'button_count') }}
                                    </li>
                                    <li class="shareItem twitter">
                                        {{ product.social_url | tw_share('none', 'Tweet', store.twitter_user, current_language.lang) }}
                                    </li>
                                    <!--<li class="shareItem pinterest">
                                        {{ product.social_url | pin_it('http:' ~ product.featured_image | product_image_url('original')) }}
                                    </li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="descriptioncol">
                        <div class="descriptioncolContent">
                            
                            <div class="row title-wrapper">
                                <div class="title">
                                    <h2 itemprop="name">{{ product.name }}</h2>
                                </div>
                                <meta itemprop="image"
                                      content="{{ 'http:' ~ product.featured_image | product_image_url('medium') }}"/>
                                <meta itemprop="url" content="{{ product.social_url }}"/>

                                {% if page_description %}
                                    <meta itemprop="description" content="{{ page_description }}"/>
                                {% endif %}

                                {% if product.sku %}
                                    <meta itemprop="sku" content="{{ product.sku }}"/>
                                {% endif %}
                                {% if product.weight %}
                                    <div itemprop="offers" itemscope itemtype="http://schema.org/QuantitativeValue"
                                         style="display:none;">
                                        <meta itemprop="unitCode"
                                              content="{{ product.weight_unit | iso_to_uncefact }}"/>
                                        <meta itemprop="value" content="{{ product.weight }}"/>
                                    </div>
                                {% endif %}
                            </div>
                            <div class="installments {% if not product.display_price or product.installments <= 1 %}hidden{% endif %}">
                                {% set installments %}<span
                                        id="installments_number">{{ product.installments }}</span>{% endset %}
                                {% if settings.no_interest %}
                                    {% set installment_amount %}<span
                                        id="installments_amount">{{ (product.price / product.installments) | money }}</span>{% endset %}
                                    {{ "En hasta {1} cuotas de {2} sin interés" | translate(installments, installment_amount) }}
                                {% else %}
                                    {{ "En hasta {1} cuotas en tarjeta de crédito" | translate(installments) }}
                                {% endif %}
                            </div>

                            <form id="product_form" class='form-inline' method="post" action="{{ store.cart_url }}">

                                <input type="hidden" name="add_to_cart" value="{{ product.id }}"/>
                                {% if product.variations %}
                                    <div class="fancyContainer row">
                                        {% for variation in product.variations %}
                                            <div class="col-md-4 col-sm-4 col-xs-4">
                                                <div class="attributeLine">
                                                    <label class="variation-label"
                                                           for="variation_{{ loop.index }}">{{ variation.name }}</label>

                                                    <select class="btn btn-default dropdown-toggle col-md-12 col-sm-12 col-xs-12"
                                                            data-toggle="dropdown" id="variation_{{ loop.index }}"
                                                            name="variation[{{ variation.id }}]"
                                                            onchange="LS.changeVariant(changeVariant, '#single-product .productContainer')">
                                                        {% for option in variation.options %}
                                                            <option value="{{ option.id }}"
                                                                    {% if product.default_options[variation.id] == option.id %}selected="selected"{% endif %}>{{ option.name }}</option>
                                                        {% endfor %}
                                                    </select>
                                                </div>
                                            </div>
                                        {% endfor %}
                                    </div>
                                {% endif %}
                                
                                <div class="addToCartButton">
                                    {% set state = store.is_catalog ? 'catalog' : (product.available ? product.display_price ? 'cart' : 'contact' : 'nostock') %}
                                    {% set texts = {'cart': "Agregar al carrito", 'contact': "Consultar precio", 'nostock': "Sin stock", 'catalog': "Consultar"} %}
                                    <input type="submit" class="btn btn-primary btn-lg addToCart {{ state }}"
                                           value="{{ texts[state] | translate }}"/>
                                </div>
                                
                                <div class="price-holder">
                                    <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                        <div class="price {% if not product.display_price %}hidden{% endif %}">
                                        <span class="price-compare">
                                            <span id="compare_price_display"
                                                  {% if not product.compare_at_price %}class="hidden"{% endif %}>{{ product.compare_at_price | money_nocents }}</span>
                                            </span>
                                            <span class="price" id="price_display" itemprop="price"
                                                  {% if not product.display_price %}class="hidden"{% endif %}>{{ product.price | money_nocents }}</span>
                                            <meta itemprop="priceCurrency" content="{{ product.currency }}"/>
                                            {% if product.stock_control %}
                                                <meta itemprop="inventoryLevel" content="{{ product.stock }}"/>
                                                <meta itemprop="availability"
                                                      href="http://schema.org/{{ product.stock ? 'InStock' : 'OutOfStock' }}"
                                                      content="{{ product.stock ? 'In stock' : 'Out of stock' }}"/>
                                            {% endif %}
                                        </div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </form>

                        </div>
                    </div>
                    {% if settings.product_description_position == "above" and product.description is not empty %}
                        <div class="row product-description-wrap">
                            <h3 class="product-description-title">Descrição</h3>
                            <p>{{ product.description }}</p>
                        </div>
                    {% endif %}
                </div> <!-- col-md-7 -->
            </div>
        </div>
    </div> <!-- row -->
    {% if settings.product_description_position == "below" and product.description is not empty %}
        <div class="row">
            <div class="col-xs-12">
                <div class="product-description-wrap">
                    <h3 class="product-description-title">{{ "Descripción del producto" | translate }}</h3>
                    <p>{{ product.description }}</p>
                </div>
            </div>
        </div>
    {% endif %}
    {% if settings.show_product_fb_comment_box %}
        <div class="row">
            <div class="col-xs-12 fb-comments" data-href="{{ product.social_url }}" data-num-posts="5" data-width="100%"></div>
        </div>
    {% endif %}
</div> <!-- container -->
{% set related_products = category.products | take(4) | shuffle %}
{% if related_products | length > 1 %}
    <div class="container products-list">
        <div class="products-header">
            <h3>{{ "Productos Relacionados" | translate }}</h3>
        </div>
        <div class="row product-row">
            {% for related in related_products %}
                {% if product.id != related.id %}
                    {% include 'snipplets/single_product.tpl' with {product : related} %}
                {% endif %}
            {% endfor %}
        </div>
    </div>
{% endif %}