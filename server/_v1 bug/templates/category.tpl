{% paginate by 12 %}

<!--<div class="container full-banner"></div>-->

<div class="container full-menu">
    <ul id="menuFull" class="">
        {% snipplet "navigation.tpl" %}
    </ul>
</div>
<div class="container products-list page">
    <div class="products-header">
        <div class="breadcrumbs-wrapper">{% snipplet "breadcrumbs.tpl" %}</div>
        <div class="title"><h2>{{ category.name }}</h2></div>
    </div>
    {% if products %}
        <div class="product-table">
            {% snipplet "product_grid.tpl" %}
        </div>
    {% else %}
        <div class="centered st">
            {{ "Próximamente" | translate }}
        </div>
    {% endif %}
    {% snipplet "pagination.tpl" %}
</div> <!-- container -->