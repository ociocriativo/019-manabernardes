<div id="shoppingCartPage" class="container page">
    <div class="headerBox-Page">
        <h2>{{ "Carrito de Compras" | translate }}</h2>
    </div>
    {% if cart.items %}
        {% if error.add %}
            <div class="alert alert-danger">{{ "No hay suficiente stock para agregar este producto al carrito." | translate }}</div>
        {% endif %}
        {% for error in error.update %}
            <div class="alert alert-danger">{{ "No podemos ofrecerte {1} unidades de {2}. Solamente tenemos {3} unidades." | translate(error.requested, error.item.name, error.stock) }}</div>
        {% endfor %}
        <form class="" action="" method="post"
              {% if store.analytics_account %}onsubmit="_gaq.push(['_linkByPost',this])"{% endif %}>
            <div class="cart-contents">
                <ul class="firstrow row">
                    <li class="col-image-product col-md-5 col-sm-6 col-xs-3 col-xxs">{{ "Producto" | translate }}</li>
                    <li class="col-quantity col-md-2 col-sm-1 col-xs-2 col-xxs">{{ "Cantidad" | translate }}</li>
                    <li class="col-price col-md-2 col-sm-2 col-xs-3 col-xxs">{{ "Precio" | translate }}</li>
                    <li class="col-subtotal col-md-2 col-sm-2 col-xs-3 col-xxs">{{ "Subtotal" | translate }}</li>
                    <li class="col-delete col-md-1 col-sm-1 col-xs-1 col-xxs last"></li>
                </ul>
                {% for item in cart.items %}
                    <ul class="productrow row">
                        <li class="col-image col-md-2 col-sm-1 col-xs-2 col-xxs">
                            <a class="thumb"
                               href="{{ item.url }}">{{ item.featured_image | product_image_url("thumb") | img_tag(item.name) }}</a>
                        </li>
                        <li class="col-product col-md-3 col-sm-5 col-xs-1 col-xxs">
                            <a class="name" href="{{ item.url }}">{{ item.name }}</a>
                        </li>
                        <li class="col-quantity col-md-2 col-sm-1 col-xs-2 col-xxs">
                            <input type="text" name="quantity[{{ item.id }}]" value="{{ item.quantity }}"/>
                        </li>
                        <li class="col-price col-md-2 col-sm-2 col-xs-3 col-xxs">
                            <span>{{ item.unit_price | money }}</span>
                        </li>
                        <li class="col-subtotal col-md-2 col-sm-2 col-xs-3 col-xxs">
                            <span>{{ item.subtotal | money }}</span>
                        </li>
                        <li class="col-delete last col-md-1 col-sm-1 col-xs-1 col-xxs">
                            <a href="#" onclick="LS.removeItem({{ item.id }})"></a>
                        </li>
                    </ul>
                {% endfor %}
                <div class='total-price'>
                    {{ "Total" | translate }}: {{ cart.total | money }}
                </div>
                {% if settings.shipping_calculator %}
                    <div id="shipping-calculator" class="text-center">
                        <ul class="list-inline">
                            <li>{{ "Ingresa aquí tu código postal para calcular tu costo de envío" | translate }}:</li>
                            <li><input type="text" name="cep" id="shipping-cep" class="form-control"/></li>
                            <li>
                                <button id="calculate-shipping-button"
                                        class="btn btn-secundary">{{ "Calcular costo de envío" | translate }}</button>
                            </li>
                        </ul>
                        <i class="loading fa fa-spinner fa-spin"></i>
                        <span class="alert alert-warning" id="invalid-cep" style="display: none;">{{ "El código postal es inválido." | translate }}</span>
                    </div>
                    <!-- #shipping-calculator -->
                {% endif %}
            </div>
            <div class="row checkOutCallOuts">
                <div class="col-xs-12">
                    <input class="btn btn-primary btn-lg col-md-4 go-to-checkout pull-right" type="submit"
                           name="go_to_checkout" value="{{ 'Iniciar Compra' | translate }}"/>
                    <input class="btn btn-secundary btn-lg pull-right" type="submit" name="update"
                           value="{{ 'Cambiar Cantidades' | translate }}" id="change-quantities" style="display:none;"/>
                </div>
            </div>
        </form>
    {% else %}
        <div class="emptyCart alert alert-danger">
            {% if error %}
                {{ "No hay suficiente stock para agregar este producto al carrito." | translate }}
            {% else %}
                {{ "El carrito de compras está vacío." | translate }}
            {% endif %}
            {{ ("Seguir comprando" | translate ~ " »") | a_tag(store.products_url) }}
        </div>
    {% endif %}
</div>