<div class="page container">
    <div class="headerBox-Page">
        <h2>{{ page.name }}</h2>
    </div>
    <hr class="featurette-divider">
    <div class="user-content">
        {{ page.content }}
    </div>
</div>