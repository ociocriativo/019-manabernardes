{% set has_banner = "banner-home.jpg" | has_custom_image %}
{% set show_help = not (settings.slider | length) and not has_banner and (not has_products or (not sections.primary.products and settings.home_display != "image")) %}

{% if settings.slider and settings.slider is not empty and (settings.home_display == "slider" or settings.home_display == "both") %}
    <div class="container full-banner">
        <ul class="full-banner-wrap">
            {% for slide in settings.slider %}
                {% set slide_img = slide.image | static_url %}
                {% if slide.link is empty %}
                    <li class="full-banner-item" style="background-image: url({{ slide_img }});">
                    </li>
                {% else %}
                    <li class="full-banner-item" style="background-image: url({{ slide_img }});">
                        <a class="full-banner-link" href="{{ slide.link }}"></a>
                    </li>
                {% endif %}
            {% endfor %}
        </ul>
    </div>
{% endif %}


<div class="container full-menu">
    <ul id="menuFull" class="">
        {% snipplet "navigation.tpl" %}
    </ul>
</div>


<div class="container products-list page">
    
    {% if show_help or (settings.home_display == "products" or settings.home_display == "both" and sections.primary.products) %}
        {% for product in sections.primary.products %}
            {% if loop.index % 4 == 1 %}
                <div class="row">
            {% endif %}

            {% include 'snipplets/single_product.tpl' %}

            {% if loop.index % 4 == 0 or loop.last %}
                </div>
            {% endif %}
        {% else %}
            {% if show_help %}
                {% for i in 1..4 %}
                    {% if loop.index % 4 == 1 %}
                        <div>
                    {% endif %}
                    
                    <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="thumbnail">
                            
                            <figure class="image-wrap show-help">
                                <a href="/admin/products/new">{{ 'img/placeholder-product.png' | static_url | img_tag }}</a>
                            </figure>
                            
                            <div class="caption">
                                <h5 class="title"><a href="/admin/products/new">{{ "Producto" | translate }}</a></h5>
                                <p class="price">{{ "$0.00" | translate }}</p>
                            </div>
                            
                        </div>
                    </div>
                    
                    {% if loop.index % 4 == 0 or loop.last %}
                        </div>
                    {% endif %}
                {% endfor %}
            {% endif %}
        {% endfor %}
    {% endif %}
</div>

<div class="container">
    <!-- widget zone-->
    
</div>