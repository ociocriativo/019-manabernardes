<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {% if template == 'home' %}
        <title>{{ store.name }}</title>
    {% else %}
        <title>{{ page_title }} &mdash; {{ store.name }}</title>
    {% endif %}
    <meta name="description" content="{{ page_description }}" />
    {% if settings.fb_admins %}
        <meta property="fb:admins" content="{{ settings.fb_admins }}" />
    {% endif %}
    {% if not store.has_custom_domain %}
        <meta property="fb:app_id" content="{{ fb_app.id }}" />
    {% endif %}
    {{ store.name | og('site_name') }}
    {% if template == 'product' %}
        {# Twitter #}
        <meta name="twitter:card" content="product" />
        <meta name="twitter:url" content="{{ product.social_url }}" />
        <meta name="twitter:image:src" content="{{ ('http:' ~ product.featured_image | product_image_url('huge')) }}" />
        {% if store.twitter_user %}
            <meta name="twitter:site" content="{{ store.twitter_user }}" />
        {% endif %}
        <meta name="twitter:title" content="{{ product.name }}" />
        <meta name="twitter:data1" content="{{ product.display_price ? product.price | money_long : 'Consultar' | translate }}" />
        <meta name="twitter:label1" content="{{ 'Precio' | translate | upper }}" />
        <meta name="twitter:data2" content="{{ product.stock_control ? (product.stock > 0 ? product.stock : 'No' | translate) : 'Si' | translate }}" />
        <meta name="twitter:label2" content="{{ 'Stock' | translate | upper }}" />
        {# Facebook #}
        {{ product.social_url | og('url') }}
        {{ product.name | og('title') }}
        {{ page_description | og('description') }}
        {{ "#{fb_app.namespace}:product" | og('type') }}
        {{ ('http:' ~ product.featured_image | product_image_url('medium')) | og('image') }}
        {{ ('https:' ~ product.featured_image | product_image_url('medium')) | og('image:secure_url') }}
        {% if product.display_price %}
            {{ (product.price / 100) | og_fb_app('price') }}
        {% endif %}
        {% if product.stock_control %}
            {{ product.stock | og_fb_app('stock') }}
        {% endif %}
    {% endif %}
    {{ '//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css' | css_tag }}
    {{ '//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css' | css_tag }}
    {{ 'css/style.scss.tpl' | static_url | css_tag }}
    {% set nojquery = true %}
    {{ '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js' | script_tag }}
    {% head_content %}

    <!--[if lt IE 9]>
    {{ "js/html5shiv.js" | static_url | script_tag }}
    <![endif]-->
</head>
<body itemscope itemtype="http://schema.org/WebPage" itemid="body">
{{back_to_admin}}

{% set is_sticky = settings.sticky_header %}

<div id="navigation" role="navigation" class="navbar navbar-default {% if is_sticky %}navbar-static-top{% else %}navbar-non-static{% endif %}" {% if is_sticky %}data-spy="affix" data-offset-top="140"{% endif %}>
    <div class="container">
		{% if has_logo %}
			<div id="logo" class="img">
				{{ store.logo | img_tag | a_tag(store.url) }}
			</div>
		{% else %}
			<div id="logo">
				<a id="no-logo" href="{{ store.url }}">{{ store.name }}</a>
			</div>
		{% endif %}
		
		<div class="message-wrapper">
			<p class="header-message">
			{% if settings.header_message %}
				{{ settings.header_message }}
			{% else %}
				&nbsp;
			{% endif %}
			</p>
		</div>
		
		<div id="cart">
			{% if not store.is_catalog %}
				{% snipplet "cart.tpl" as "cart" %}
			{% endif %}
		</div>
		
		<div id="searchWrap" class="search-content-wrapper">
			<form action="{{ store.search_url }}" method="get">
				<div class="input-group search-wrapper">
					<input class="text-input form-control" type="text" name="q" placeholder="{{ 'Buscar...' | translate }}"/>
					<span class="input-group-btn">
					<button class="btn btn-search" type="submit"></button>
					</span>
				</div>
			</form>
		</div>
		
		<div id="accountWrap" class="account-wrapper">
			{% if store.has_accounts %}
				<ul id="auth" class="list-inline">
					{% if not customer %}
						{% if store.customer_accounts != 'mandatory' %}
							<li>{{ "Crear cuenta" | translate | a_tag(store.customer_register_url) }}</li>
						{% endif %}
						{{ "Iniciar sesión" | translate | a_tag(store.customer_login_url) }}
					{% else %}
						<li>{{ "Mi cuenta" | translate | a_tag(store.customer_home_url) }}</li>
						<li>{{ "Cerrar sesión" | translate | a_tag(store.customer_logout_url) }}</li>
					{% endif %}
				</ul>
			{% endif %}
		</div>
		
    </div>
</div>
<div id="wrap">
	{% template_content %}
</div>

<!-- FOOTER -->
<footer>
    {% set has_shipping_payment_logos = settings.payments or settings.shipping %}
    {% if has_shipping_payment_logos %}
        <div class="container">
            <hr class="featurette-divider">
            <div class="row pre-footer">
                <div class="col-xs-6 payment">
                    <ul class="list-inline">
                        {% for payment in settings.payments %}
                            <li>{{ payment | payment_logo | img_tag('', {'height' : 28}) }}</li>
                        {% endfor %}
                    </ul>
                </div>
                <div class="col-xs-6 delivery">
                    <ul class="list-inline">
                        {% for shipping in settings.shipping %}
                            <li>{{ shipping | shipping_logo | img_tag('', {'height' : 28}) }}</li>
                        {% endfor %}
                    </ul>
                </div>
            </div>
            <!-- /.pre-footer-->
        </div>
        <!-- /.container-->
    {% endif %}
    <div class="footer">
        <div class="footer-nav">
            <div class="container">
                <ul class="list-inline pull-right seals" {% if not (store.afip or ebit or siteforte) %}style="display: none;"{% endif %}>
                    {% if ebit %}
                        <li class="ebit">
                            {{ ebit }}
                        </li>
                    {% endif %}
                    {% if siteforte %}
                        <li class="siteforte text-right">
                            {{ siteforte }}
                        </li>
                    {% endif %}
                    {% if store.afip %}
                        <li class="afip">
                            {{ store.afip | raw }}
                        </li>
                    {% endif %}
                </ul>
                <nav>
                    <ul class="list-inline">
                        {% snipplet "navigation-foot.tpl" %}
                    </ul>
                </nav>
                <p class="copy">{{ "Copyright {1} - {2}. Todos los derechos reservados." | translate( (store.business_name ? store.business_name : store.name) ~ (store.business_id ? ' - ' ~ store.business_id : ''), "now" | date('Y') ) }}</p>
            </div>
            <!-- /.container-->
        </div>
        <!-- /.footer-nav-->
        {% set has_social = store.facebook or store.twitter or store.google_plus or store.pinterest or store.instagram %}
        
    </div>
    <!-- /.footer-->
</footer>
{{ '//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js' | script_tag }}
{{ '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js' | script_tag }}
{{ '//maps.google.com/maps/api/js?sensor=true' | script_tag }}
{{ 'js/habitus.js' | static_url | script_tag }}
<script type="text/javascript">
    function changeVariant(variant){
        var parent = $("body");
        if (variant.element){
            parent = $(variant.element);
        }

        var sku = parent.find('#sku');
        if(sku.length) {
            sku.text(variant.sku).show();
        }

        if (variant.price_short){
            parent.find('#price_display').text(variant.price_short).show();
        } else {
            parent.find('#price_display').hide();
        }

        if (variant.compare_at_price_short){
            parent.find('#compare_price_display').text(variant.compare_at_price_short).show();
        } else {
            parent.find('#compare_price_display').hide();
        }

        if (variant.installments && variant.installments > 1){
            parent.find('#installments_number').text(variant.installments);
            parent.find('#installments_amount').text(variant.installments_amount_short);
            parent.find('.installments').show();
        } else {
            parent.find('.installments').hide();
        }

        if(variant.contact) {
            parent.find('.social-container-box').hide();
        } else {
            parent.find('.social-container-box').show();
        }

        var button = parent.find('.addToCart');
        button.removeClass('cart').removeClass('contact').removeClass('nostock');
        {% if not store.is_catalog %}
        if (!variant.available){
            button.val('{{ "Sin stock" | translate }}');
            button.addClass('nostock');
            button.attr('disabled', 'disabled');
        } else if (variant.contact) {
            button.val('{{ "Consultar precio" | translate }}');
            button.addClass('contact');
            button.removeAttr('disabled');
        } else {
            button.val('{{ "Agregar al carrito" | translate }}');
            button.addClass('cart');
            button.removeAttr('disabled');
        }
        {% endif %}
    }

    $(document).ready(function() {
        function centerImage(container) {
            var imageHeight = container.find('img').height();
            wrapperHeight = container.height();
            overlap = (wrapperHeight - imageHeight) / 2;
            container.find('img').css('margin-top', overlap);
        }

        $(window).on("load resize", function() {centerImage($('.image-wrap'));});

        $('.image-wrap').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',
            function() {
                centerImage($(this));
            }
        );

        $('#menu').supersubs({
            minWidth:    20,
            maxWidth:    40,
            extraWidth:  1
        }).superfish({
            animation:   {opacity:'show',height:'show'},
            animationOut:  {opacity:'hide',height:'hide'},
            autoArrows: false,
            dropShadows: false,
            speed: 'fast',
            speedOut: 'fast',
            delay: 100
        });

        $('#product_form').submit(function(){
            var button = $(this).find(':submit');

            button.attr('disabled', 'disabled');
            if (button.hasClass('cart')){
                button.val('{{ "Agregando..." | translate }}');
            }
        });

        {% if contact and contact.success and contact.type == 'newsletter' %}
        var $newsletter = $('#newsletter');
        $newsletter.find('form').hide();
        $newsletter.find('.title').hide();
        $newsletter.find('#ofertas').hide();
        {% endif %}

        $("#tS3").thumbnailScroller({
            scrollerType:"clickButtons",
            scrollerOrientation:"vertical",
            scrollEasing:"easeOutCirc",
            scrollEasingAmount:600,
            acceleration:4,
            scrollSpeed:800,
            noScrollCenterSpace:10,
            autoScrolling:0,
            autoScrollingSpeed:2000,
            autoScrollingEasing:"easeInOutQuad",
            autoScrollingDelay:500
        });

        $(".col-quantity input").keypress(function () {
            $('#change-quantities').show();
        });
        var $calculator = $("#shipping-calculator");
        var $loading = $calculator.find(".loading");
        $("#calculate-shipping-button").click(function () {
            params = {'cep': $("#shipping-cep").val()};
            $loading.show();
            $("#invalid-cep").hide();
            $.post('{{store.shipping_calculator_url | escape('js')}}', params, function (data) {
                $loading.hide();
                if (data.success) {
                    $calculator.html(data.html);
                } else {
                    $("#invalid-cep").show();
                }
            }, 'json');
            return false;
        });
        $loading.hide();

        {% if store.address %}
            $('#google-map').gmap3({
                getlatlng:{
                    address: "{{ store.address | escape('js') }}",
                    callback: function(results) {
                        if ( !results ) return;
                        storeLocation = results[0].geometry.location
                        $('#google-map').gmap3({
                            map: {
                                options: {
                                    zoom: 14,
                                    center: storeLocation
                                }
                            },
                            marker: {
                                address: "{{ store.address | escape('js') }}"
                            }
                        });
                    }
                }
            })
        {% endif %}

        var $alert = $(".alert:first");
        if($alert.length) {
            $('html,body').animate({scrollTop:$alert.offset().top}, 400);
        }
		
		
		
		var bxslider = $('.homeslider');
		
        /*var bxslider = $('.homeslider').bxSlider({
            auto: {% if settings.slider_auto and settings.slider | length > 1 %}true{% else %}false{% endif %},
            pause: 5000,
            autoHover: true,
            adaptiveHeight: false,
			pager : false
        });*/

        {% if settings.slider | length == 1 %}
            $('.bx-pager').remove();
        {% endif %}

        {% if provinces_json %}
        $('select[name="country"]').change(function() {
            var provinces = {{provinces_json | default('{}') | raw}};
            LS.swapProvinces(provinces[$(this).val()]);
        }).change();
        {% endif %}
		
		
		// CUSTOM
		$('#menuFull .full-menu-item').hover(function() {
			$(this).find('.full-menu-sub').fadeIn(200);
		}, function() {
			$(this).find('.full-menu-sub').fadeOut(200);
		});
		
		
		
    });
</script>
{% if settings.infinite_scrolling and (template == 'category' or template == 'search') %}
    <script type="text/javascript">
        $(function() {
            new LS.infiniteScroll({
                afterSetup: function() {
                    $('.crumbPaginationContainer').hide();
                },
                // afterLoaded: function() {
                //     $('.fancybox').fancybox();
                // },
                productGridClass: 'product-table',
                bufferPx: 50,
                productsPerPage: 12
            });
        });
    </script>
{% endif %}
{% if template == 'product' %}
    <script type="text/javascript">
        {% if settings.show_product_fb_comment_box %}
        window.fbAsyncInit = function() {
            FB.Event.subscribe('comment.create',
                    function (response) {
                        LS.commentNotify("{{store.fb_comment_url}}", {{product.id}}, response);
                    });
        };
        {% endif %}
    </script>
{% endif %}
{{ social_js }}
{% if store.live_chat %}
    <!-- begin olark code --><script type='text/javascript'>/*{literal}<![CDATA[*/
    window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){f[z]=function(){(a.s=a.s||[]).push(arguments)};var a=f[z]._={},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={0:+new Date};a.P=function(u){a.p[u]=new Date-a.p[0]};function s(){a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{b.contentWindow[g].open()}catch(w){c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{var t=b.contentWindow[g];t.write(p());t.close()}catch(x){b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
    /* custom configuration goes here (www.olark.com/documentation) */
    olark.identify('{{store.live_chat | escape('js')}}');/*]]>{/literal}*/
</script>
<!-- end olark code -->
{% endif %}
{{ store.assorted_js | raw }}
</body>
</html>